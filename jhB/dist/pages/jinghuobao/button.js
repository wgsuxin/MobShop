"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Buttonfab = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Buttonfab, _BaseComponent);

  function Buttonfab() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Buttonfab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Buttonfab.__proto__ || Object.getPrototypeOf(Buttonfab)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["showView"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Buttonfab, [{
    key: "_constructor",
    value: function _constructor() {
      _get(Buttonfab.prototype.__proto__ || Object.getPrototypeOf(Buttonfab.prototype), "_constructor", this).apply(this, arguments);
      this.state = {
        showView: true
      };
      this.onChangeShowState = this.onChangeShowState.bind(this);
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "onChangeShowState",
    value: function onChangeShowState() {
      this.setState({
        showView: !this.state.showView
      });
    }
  }, {
    key: "toindex",
    value: function toindex() {
      _index2.default.switchTab({
        url: '/pages/jinghuobao/Index'
      });
    }
  }, {
    key: "tosearch",
    value: function tosearch() {
      _index2.default.navigateTo({
        url: '/pages/jinghuobao/Search'
      });
    }
  }, {
    key: "toclassify",
    value: function toclassify() {
      _index2.default.switchTab({
        url: '/pages/jinghuobao/Classify'
      });
    }
  }, {
    key: "toshopcar",
    value: function toshopcar() {
      _index2.default.switchTab({
        url: '/pages/jinghuobao/shop'
      });
    }
  }, {
    key: "tohome",
    value: function tohome() {
      _index2.default.switchTab({
        url: '/pages/jinghuobao/home'
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;
      Object.assign(this.__state, {});
      return this.__state;
    }
  }]);

  return Buttonfab;
}(_index.Component), _class.properties = {}, _class.$$events = ["onChangeShowState", "toindex", "tosearch", "toclassify", "toshopcar", "tohome"], _temp2);
exports.default = Buttonfab;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Buttonfab));