"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _md = require("../../../npm/md5/md5.js");

var _md2 = _interopRequireDefault(_md);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var head = "/pages/jinghuobao/img/login/head.png";
var head1 = "/pages/jinghuobao/img/logins/head.png";
var icon_user = "/pages/jinghuobao/img/login/icon_user.png";
var icon_user1 = "/pages/jinghuobao/img/logins/icon_user.png";
var icon_del = "/pages/jinghuobao/img/login/icon_del.png";
var icon_del1 = "/pages/jinghuobao/img/logins/icon_del.png";
var icon_pwd = "/pages/jinghuobao/img/login/icon_pwd.png";
var icon_pwd1 = "/pages/jinghuobao/img/logins/icon_pwd.png";
var icon_pwd_switch = "/pages/jinghuobao/img/login/icon_pwd_switch.png";
var icon_pwd_switch1 = "/pages/jinghuobao/img/logins/icon_pwd_switch.png";
var qq = "/pages/jinghuobao/img/login/qq.png";
var qq1 = "/pages/jinghuobao/img/logins/qq.png";
var wechat = "/pages/jinghuobao/img/login/wechat.png";
var wechat1 = "/pages/jinghuobao/img/logins/wechat.png";
var weibo = "/pages/jinghuobao/img/login/weibo.png";
var weibo1 = "/pages/jinghuobao/img/logins/weibo.png";

var Login = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Login, _BaseComponent);

  function Login() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Login);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Login.__proto__ || Object.getPrototypeOf(Login)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "username", "userpwd", "pwdType", "imgInfo", "showProvider"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Login, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Login.prototype.__proto__ || Object.getPrototypeOf(Login.prototype), "_constructor", this).call(this, props);
      var isUni = typeof uni !== 'undefined';
      this.state = {
        username: '',
        userpwd: '',
        pwdType: 'password',
        imgInfo: {
          head: isUni ? head : head1,
          icon_user: isUni ? icon_user : icon_user1,
          icon_del: isUni ? icon_del : icon_del1,
          icon_pwd: isUni ? icon_pwd : icon_pwd1,
          icon_pwd_switch: isUni ? icon_pwd_switch : icon_pwd_switch1,
          qq: isUni ? qq : qq1,
          wechat: isUni ? wechat : wechat1,
          weibo: isUni ? weibo : weibo1
        },
        showProvider: {
          weixin: false,
          qq: false,
          sinaweibo: false,
          xiaomi: false
        }
      };
      this.findPwd = this.findPwd.bind(this);
      this.goReg = this.goReg.bind(this);
      this.login = this.login.bind(this);
      this.switchPwd = this.switchPwd.bind(this);
      this.delUser = this.delUser.bind(this);
      this.handelvalue = this.handelvalue.bind(this);
      this.handelpwd = this.handelpwd.bind(this);
    }
  }, {
    key: "findPwd",
    value: function findPwd() {
      _index2.default.navigateTo({
        url: '/pages/jinghuobao/login/resetpasswd'
      });
    }
  }, {
    key: "goReg",
    value: function goReg() {
      _index2.default.navigateTo({
        url: '/pages/jinghuobao/login/register'
      });
    }
  }, {
    key: "handelvalue",
    value: function handelvalue(e) {
      var uservalue = e.target.value;
      this.setState({
        username: uservalue
      });
    }
  }, {
    key: "handelpwd",
    value: function handelpwd(e) {
      var handelpwd = e.target.value;
      this.setState({
        userpwd: handelpwd
      });
    }
  }, {
    key: "login",
    value: function login() {
      var _this2 = this;

      var username = this.state.username;
      var userpwd = this.state.userpwd;
      if (!/^1(3|4|5|6|7|8|9)\d{9}$/.test(username)) {
        _index2.default.showToast({ title: '请填写正确手机号码', icon: "none" });
        return false;
      }
      _index2.default.showLoading({
        title: '提交中...'
      });
      setTimeout(function () {
        var md5PW = (0, _md2.default)(_this2.state.userpwd);
        _index2.default.getStorage({
          key: 'UserList',
          success: function success(res) {
            console.log(res);
            for (var i in res.data) {
              var row = res.data[i];
              console.log(row);
              if (row.username == _this2.state.phoneNumber) {
                _index2.default.hideLoading();
                //比对密码
                if (md5PW == res.data[i].userpwd) {
                  _index2.default.showToast({ title: '登录成功', icon: "success" });
                } else {
                  _index2.default.showToast({ title: '账号或密码不正确', icon: "none" });
                }
              }
            }
          },
          fail: function fail(e) {
            _index2.default.hideLoading();
            _index2.default.showToast({ title: '手机号码未注册', icon: "none" });
          }
        });
      }, 1000);
    }
  }, {
    key: "switchPwd",
    value: function switchPwd() {}
  }, {
    key: "delUser",
    value: function delUser() {
      this.setState({
        username: ''
      });
    }
  }, {
    key: "thirdLogin",
    value: function thirdLogin(provider) {

      _index2.default.showLoading();
      //第三方登录
      _index2.default.login({
        provider: provider,
        success: function success(res) {
          console.log("success: " + JSON.stringify(res));
          //案例直接获取用户信息，一般不是在APP端直接获取用户信息，比如微信，获取一个code，传递给后端，后端再去请求微信服务器获取用户信息
          _index2.default.getUserInfo({
            provider: provider,
            success: function success(res) {
              console.log('用户信息：' + JSON.stringify(res.userInfo));
              _index2.default.setStorage({
                key: 'UserInfo',
                data: {
                  username: res.userInfo.nickName,
                  face: res.userInfo.avatarUrl,
                  signature: '个性签名',
                  integral: 0,
                  balance: 0,
                  envelope: 0
                },
                success: function success() {
                  _index2.default.hideLoading();
                  _index2.default.showToast({ title: '登录成功', icon: "success" });
                  setTimeout(function () {
                    _index2.default.navigateBack();
                  }, 300);
                }
              });
            }
          });
        },
        fail: function fail(e) {
          console.log("fail: " + JSON.stringify(e));
        }
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;
      var anonymousState__temp = (0, _index.internal_inline_style)({ width: '55px', height: '65px' });
      var anonymousState__temp2 = (0, _index.internal_inline_style)({ width: '20px', height: '20px' });
      var anonymousState__temp3 = (0, _index.internal_inline_style)({ width: '20px', height: '20px' });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3
      });
      return this.__state;
    }
  }]);

  return Login;
}(_index.Component), _class.properties = {}, _class.$$events = ["handelvalue", "delUser", "handelpwd", "switchPwd", "login", "goReg", "findPwd", "thirdLogin"], _temp2);
exports.default = Login;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Login, true));