"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Register = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Register, _BaseComponent);

  function Register() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Register);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Register.__proto__ || Object.getPrototypeOf(Register)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "phoneNumber", "code", "passwd", "getCodeText", "getCodeBtnColor", "getCodeisWaiting"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Register, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Register.prototype.__proto__ || Object.getPrototypeOf(Register.prototype), "_constructor", this).call(this, props);
      this.state = {
        phoneNumber: "",
        code: '',
        passwd: "",
        getCodeText: '获取验证码',
        getCodeBtnColor: "#ffffff",
        getCodeisWaiting: false
      };
      this.toLogin = this.toLogin.bind(this);
      this.doReg = this.doReg.bind(this);
      this.getCode = this.getCode.bind(this);
      this.handphn = this.handphn.bind(this);
      this.setTimer = this.setTimer.bind(this);
      this.Timer = this.Timer.bind(this);
    }
  }, {
    key: "Timer",
    value: function Timer() {}
  }, {
    key: "toLogin",
    value: function toLogin() {}
  }, {
    key: "doReg",
    value: function doReg() {
      _index2.default.navigateTo({
        url: '/pages/jinghuobao/login/login'
      });
    }
  }, {
    key: "handphn",
    value: function handphn(e) {
      var valuephn = e.target.value;
      this.setState({
        phoneNumber: valuephn
      });
    }
  }, {
    key: "getCode",
    value: function getCode() {
      var _this2 = this;

      _index2.default.hideKeyboard();
      if (this.getCodeisWaiting) {
        return;
      }
      if (!/^1[345789]\d{9}$/.test(this.state.phoneNumber)) {
        _index2.default.showToast({ title: '请填写正确手机号码', icon: "none" });
        return false;
      }

      this.setState({
        getCodeText: "发送中...",
        getCodeisWaiting: true,
        getCodeBtnColor: "rgba(255,255,255,0.5)"
      });

      //示例用定时器模拟请求效果
      setTimeout(function () {
        _index2.default.showToast({ title: '验证码已发送', icon: "none" });
        //示例默认1234，生产中请删除这一句。
        _this2.code = 1234;
        _this2.setTimer();
      }, 1000);
    }
  }, {
    key: "setTimer",
    value: function setTimer() {
      var _this3 = this;

      var holdTime = 60;
      this.setState({
        getCodeText: "重新获取(60)"
      });

      this.Timer = setInterval(function () {
        if (holdTime <= 0) {
          _this3.setState({
            getCodeText: "获取验证码",
            getCodeisWaiting: false,
            getCodeBtnColor: "#ffffff"
          });

          clearInterval(_this3.Timer);
          return;
        }
        _this3.setState({
          getCodeText: "重新获取(" + holdTime + ")"
        });
        holdTime--;
      }, 1000);
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;
      var anonymousState__temp = (0, _index.internal_inline_style)({ 'color': 'getCodeBtnColor' });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp
      });
      return this.__state;
    }
  }]);

  return Register;
}(_index.Component), _class.properties = {}, _class.$$events = ["getCode", "handphn", "doReg", "toLogin"], _temp2);
exports.default = Register;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Register, true));