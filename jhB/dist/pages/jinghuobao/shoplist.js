"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var right = "/pages/jinghuobao/img/right.png";

var Shoplist = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Shoplist, _BaseComponent);

  function Shoplist() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Shoplist);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Shoplist.__proto__ || Object.getPrototypeOf(Shoplist)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "right", "dargStyle", "downDragStyle", "downText", "upDragStyle", "pullText", "start_p", "scrollY", "dargState"], _this.config = {
      navigationBarTitleText: '店铺列表'
    }, _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Shoplist, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Shoplist.prototype.__proto__ || Object.getPrototypeOf(Shoplist.prototype), "_constructor", this).call(this, props);
      this.state = {
        dargStyle: { //下拉框的样式
          top: "0px"
        },
        downDragStyle: { //下拉图标的样式
          height: "0px"
        },
        downText: '下拉刷新',
        upDragStyle: { //上拉图标样式
          height: "0px"
        },
        pullText: '上拉加载更多',
        start_p: {},
        scrollY: true,
        dargState: 0 //刷新状态 0不做操作 1刷新 -1加载更多
      };
    }
  }, {
    key: "reduction",
    value: function reduction() {
      var _this2 = this;

      //还原初始设置
      var time = 0.5;
      this.setState({
        upDragStyle: { //上拉图标样式
          height: "0px",
          transition: "all " + time + "s"
        },
        dargState: 0,
        dargStyle: {
          top: "0px",
          transition: "all " + time + "s"
        },
        downDragStyle: {
          height: "0px",
          transition: "all " + time + "s"
        },
        scrollY: true
      });
      setTimeout(function () {
        _this2.setState({
          dargStyle: {
            top: "0px"
          },
          upDragStyle: { //上拉图标样式
            height: "0px"
          },
          pullText: '上拉加载更多',
          downText: '下拉刷新'
        });
      }, 500);
    }
  }, {
    key: "touchStart",
    value: function touchStart(e) {
      this.setState({
        start_p: e.touches[0]
      });
    }
  }, {
    key: "touchmove",
    value: function touchmove(e) {
      var that = this;
      var move_p = e.touches[0],

      //移动时的位置
      deviationX = 0.30,

      //左右偏移量(超过这个偏移量不执行下拉操作)
      deviationY = 70,

      //拉动长度（低于这个值的时候不执行）
      maxY = 100; //拉动的最大高度

      var start_x = this.state.start_p.clientX,
          start_y = this.state.start_p.clientY,
          move_x = move_p.clientX,
          move_y = move_p.clientY;

      //得到偏移数值
      var dev = Math.abs(move_x - start_x) / Math.abs(move_y - start_y);
      if (dev < deviationX) {
        //当偏移数值大于设置的偏移数值时则不执行操作
        var pY = Math.abs(move_y - start_y) / 3.5; //拖动倍率（使拖动的时候有粘滞的感觉--试了很多次 这个倍率刚好）
        if (move_y - start_y > 0) {
          //下拉操作
          if (pY >= deviationY) {
            this.setState({ dargState: 1, downText: '释放刷新' });
          } else {
            this.setState({ dargState: 0, downText: '下拉刷新' });
          }
          if (pY >= maxY) {
            pY = maxY;
          }
          this.setState({
            dargStyle: {
              top: pY + 'px'
            },
            downDragStyle: {
              height: pY + 'px'
            },
            scrollY: false //拖动的时候禁用
          });
        }
        if (start_y - move_y > 0) {
          //上拉操作
          console.log('上拉操作');
          if (pY >= deviationY) {
            this.setState({ dargState: -1, pullText: '释放加载更多' });
          } else {
            this.setState({ dargState: 0, pullText: '上拉加载更多' });
          }
          if (pY >= maxY) {
            pY = maxY;
          }
          this.setState({
            dargStyle: {
              top: -pY + 'px'
            },
            upDragStyle: {
              height: pY + 'px'
            },
            scrollY: false //拖动的时候禁用
          });
        }
      }
    }
  }, {
    key: "pull",
    value: function pull() {
      //上拉
      console.log('上拉');
      // this.props.onPull()
    }
  }, {
    key: "down",
    value: function down() {
      //下拉
      console.log('下拉');
      // this.props.onDown()
    }
  }, {
    key: "ScrollToUpper",
    value: function ScrollToUpper() {
      //滚动到顶部事件
      console.log('滚动到顶部事件');
      // this.props.Upper()
    }
  }, {
    key: "ScrollToLower",
    value: function ScrollToLower() {
      //滚动到底部事件
      console.log('滚动到底部事件');
      // this.props.Lower()
    }
  }, {
    key: "touchEnd",
    value: function touchEnd(e) {
      if (this.state.dargState === 1) {
        this.down();
      } else if (this.state.dargState === -1) {
        this.pull();
      }
      this.reduction();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;

      var dargStyle = this.__state.dargStyle;
      var downDragStyle = this.__state.downDragStyle;
      var upDragStyle = this.__state.upDragStyle;

      var anonymousState__temp = (0, _index.internal_inline_style)(downDragStyle);
      var anonymousState__temp2 = (0, _index.internal_inline_style)(dargStyle);
      var anonymousState__temp3 = (0, _index.internal_inline_style)(upDragStyle);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        right: right
      });
      return this.__state;
    }
  }]);

  return Shoplist;
}(_index.Component), _class.properties = {}, _class.$$events = ["touchmove", "touchEnd", "touchStart", "ScrollToUpper", "ScrollToLower"], _temp2);
exports.default = Shoplist;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Shoplist, true));