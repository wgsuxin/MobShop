"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var face1 = "/pages/jinghuobao/img/im/face/face_1.jpg";
var face2 = "/pages/jinghuobao/img/im/face/face_2.jpg";
var face3 = "/pages/jinghuobao/img/im/face/face_3.jpg";
var face4 = "/pages/jinghuobao/img/im/face/face_4.jpg";
var face5 = "/pages/jinghuobao/img/im/face/face_5.jpg";
var face6 = "/pages/jinghuobao/img/im/face/face_6.jpg";
var face7 = "/pages/jinghuobao/img/im/face/face_7.jpg";
var face8 = "/pages/jinghuobao/img/im/face/face_8.jpg";
var face9 = "/pages/jinghuobao/img/im/face/face_9.jpg";
var face10 = "/pages/jinghuobao/img/im/face/face_10.jpg";
var face11 = "/pages/jinghuobao/img/im/face/face_11.jpg";
var face12 = "/pages/jinghuobao/img/im/face/face_12.jpg";
var face13 = "/pages/jinghuobao/img/im/face/face_13.jpg";
var face15 = "/pages/jinghuobao/img/im/face/face_15.jpg";

var Msg = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Msg, _BaseComponent);

  function Msg() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Msg);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Msg.__proto__ || Object.getPrototypeOf(Msg)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["chatList"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Msg, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Msg.prototype.__proto__ || Object.getPrototypeOf(Msg.prototype), "_constructor", this).call(this, props);
      this.state = {
        chatList: [{
          uid: 1,
          username: "鲜果蔬专营店",
          face: face1,
          time: "13:08",
          msg: "亲，20点前下单都是当天送达的",
          tisNum: 1
        }, {
          uid: 2,
          username: "官店大欺客旗舰店",
          face: face2,
          time: "13:05",
          msg: "问那么多干什么？不想买就滚~",
          tisNum: 0
        }, {
          uid: 3,
          username: "妙不可言",
          face: face3,
          time: "12:15",
          msg: "推荐一个商品呗？",
          tisNum: 0
        }, {
          uid: 4,
          username: "茶叶专卖",
          face: face4,
          time: "12:11",
          msg: "现在卖的都是未过青的茶哦",
          tisNum: 0
        }, {
          uid: 5,
          username: "likeKiss客服",
          face: face5,
          time: "12:10",
          msg: "你好，发福建快递多久送到的？",
          tisNum: 0
        }, {
          uid: 6,
          username: "白开水",
          face: face6,
          time: "12:10",
          msg: "在吗？",
          tisNum: 0
        }, {
          uid: 7,
          username: "衣帽间的叹息",
          face: face7,
          time: "11:56",
          msg: "新品上市，欢迎选购",
          tisNum: 0
        }, {
          uid: 8,
          username: "景萧疏",
          face: face8,
          time: "11:56",
          msg: "商品七天无理由退换的",
          tisNum: 0
        }, {
          uid: 9,
          username: "文宁先生",
          face: face9,
          time: "12:15",
          msg: "星期天再发货的",
          tisNum: 0
        }, {
          uid: 10,
          username: "高端Chieh",
          face: face10,
          time: "12:36",
          msg: "建议你直接先测量好尺码在选购的。",
          tisNum: 0
        }, {
          uid: 11,
          username: "mode旗舰店",
          face: face11,
          time: "12:40",
          msg: "新品5折，还有大量优惠券。",
          tisNum: 0
        }, {
          uid: 12,
          username: "敏萘客服",
          face: face12,
          time: "12:36",
          msg: "还没有用，等我明天早上试一下",
          tisNum: 0
        }, {
          uid: 13,
          username: "春天里的花",
          face: face13,
          time: "12:36",
          msg: "适用于成年人的，小孩不适用的",
          tisNum: 0
        }, {
          uid: 14,
          username: "电脑外设专业户",
          face: face13,
          time: "12:36",
          msg: "把上面的螺丝拆下来，把铁片撬开就能看见了",
          tisNum: 0
        }, {
          uid: 15,
          username: "至善汽车用品",
          face: face15,
          time: "12:36",
          msg: "这个产品是原车配件，完美装上的",
          tisNum: 0
        }]
      };
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;

      var chatList = this.__state.chatList;

      Object.assign(this.__state, {});
      return this.__state;
    }
  }]);

  return Msg;
}(_index.Component), _class.properties = {}, _class.$$events = [], _temp2);
exports.default = Msg;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Msg, true));