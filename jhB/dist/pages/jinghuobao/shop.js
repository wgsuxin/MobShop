"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var p1 = "/pages/jinghuobao/img/goods/p1.jpg";
var p2 = "/pages/jinghuobao/img/goods/p2.jpg";
var p3 = "/pages/jinghuobao/img/goods/p3.jpg";
var p4 = "/pages/jinghuobao/img/goods/p4.jpg";
var p5 = "/pages/jinghuobao/img/goods/p5.jpg";

var Shop = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Shop, _BaseComponent);

  function Shop() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Shop);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Shop.__proto__ || Object.getPrototypeOf(Shop)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "goodsList", "shuliang", "sumPrice", "headerPosition", "headerTop", "statusTop", "selectedList", "isAllselected", "theIndex", "oldIndex", "isStop"], _this.config = {
      navigationBarTitleText: '进货车'
    }, _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Shop, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Shop.prototype.__proto__ || Object.getPrototypeOf(Shop.prototype), "_constructor", this).call(this, props);
      this.state = {

        shuliang: 1,
        sumPrice: '0.00',
        headerPosition: "fixed",
        headerTop: null,
        statusTop: null,
        selectedList: [],
        isAllselected: false,
        goodsList: [{ id: 1, img: p1, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }, { id: 2, img: p2, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127, number: 1, selected: false }, { id: 3, img: p3, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 128.5, number: 1, selected: false }, { id: 4, img: p4, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 129.5, number: 1, selected: false }, { id: 5, img: p5, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 126.5, number: 1, selected: false }],
        theIndex: null,
        oldIndex: null,
        isStop: false
      };
      this.delc = this.delc.bind(this);
      this.infc = this.infc.bind(this);
      this.deleteList = this.deleteList.bind(this);
      this.choose = this.choose.bind(this);
      this.allSelect = this.allSelect.bind(this);
      this.toConfirmation = this.toConfirmation.bind(this);
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "choose",
    value: function choose(num) {
      var _this2 = this;

      var index = num.currentTarget.dataset.index;
      var itemselected = [].concat(_toConsumableArray(this.state.goodsList));
      itemselected[index].selected = !itemselected[index].selected;
      this.setState({
        itemselected: itemselected
      }, function () {});
      console.log("itemselected");
      var i = this.state.selectedList.indexOf(this.state.goodsList[index].id);

      if (i > -1) {
        var itemlist = this.state.selectedList;
        itemlist.splice(i, 1);
        this.setState({
          selectedList: itemlist
        }, function () {
          console.log("this.state.selectedList.length");
          if (_this2.state.selectedList.length === _this2.state.goodsList.length) {
            _this2.setState({
              isAllselected: !_this2.state.isAllselected
            });
            console.log(_this2.state.isAllselected);
          } else {
            _this2.setState({
              isAllselected: false
            });

            console.log(_this2.state.goodsList.length);
          }
        });
      } else {
        this.setState({
          selectedList: this.state.selectedList.concat(this.state.goodsList[index].id)
        }, function () {
          if (_this2.state.selectedList.length === _this2.state.goodsList.length) {
            _this2.setState({
              isAllselected: !_this2.state.isAllselected
            });
          } else {
            _this2.setState({
              isAllselected: false
            });
          }
        });
      }

      this.sum();
    }
    //全选

  }, {
    key: "allSelect",
    value: function allSelect() {
      var _this3 = this;

      var len = this.state.goodsList.length;
      var arr = [];

      var _loop = function _loop(i) {
        var itemgoodsList = [].concat(_toConsumableArray(_this3.state.goodsList));
        itemgoodsList[i].selected = _this3.state.isAllselected ? false : true;
        console.log(_this3.state.isAllselected);
        _this3.setState({
          itemgoodsList: itemgoodsList
        }, function () {
          console.log(itemgoodsList);
        });
        arr.push(_this3.state.goodsList[i].id);
      };

      for (var i = 0; i < len; i++) {
        _loop(i);
      }
      var itemselectedList = this.state.selectedList;
      itemselectedList = this.state.isAllselected ? [] : arr;
      this.setState({
        itemselectedList: itemselectedList
      });
      this.setState({
        isAllselected: this.state.isAllselected || this.state.goodsList.length == 0 ? false : true
      });
      this.sum();
    }
  }, {
    key: "delc",
    value: function delc(num, e) {

      var index = num.currentTarget.dataset.index;
      var item = [].concat(_toConsumableArray(this.state.goodsList));
      if (item[index].number > 1) {
        item[index].number = item[index].number - 1;
        this.setState({
          item: item
        });
      }
      this.sum();
    }
  }, {
    key: "infc",
    value: function infc(num) {
      var index = num.currentTarget.dataset.index;
      var item = [].concat(_toConsumableArray(this.state.goodsList));
      item[index].number = item[index].number + 1;
      this.setState({
        item: item
      });

      this.sum();
    }
    //顶部删除商品

  }, {
    key: "deleteGoods",
    value: function deleteGoods(id) {
      var len = this.state.goodsList.length;
      for (var i = 0; i < len; i++) {
        if (id == this.state.goodsList[i].id) {
          this.state.goodsList.splice(i, 1);
          break;
        }
      }
      this.sum();
      this.state.oldIndex = null;
      this.state.theIndex = null;
    }
    //下部删除商品

  }, {
    key: "deleteList",
    value: function deleteList() {
      var len = this.state.selectedList.length;
      for (var i = 0; i < len; i++) {
        this.deleteGoods(this.state.selectedList[i]);
      }
      this.state.selectedList = [];
      this.state.isAllselected = this.state.selectedList.length == this.state.goodsList.length && this.state.goodsList.length > 0;
      this.sum();
    }
    // 合计

  }, {
    key: "sum",
    value: function sum() {
      this.state.sumPrice = 0;
      var len = this.state.goodsList.length;
      for (var i = 0; i < len; i++) {
        if (this.state.goodsList[i].selected) {
          this.state.sumPrice = this.state.sumPrice + this.state.goodsList[i].number * this.state.goodsList[i].price;
          this.setState({
            sumPrice: this.state.sumPrice
          });
        }
      }
      var sumPrice = this.state.sumPrice;
      sumPrice.toFixed(2);
      this.setState({
        sumPrice: sumPrice
      });
    }
    //跳转确认订单结算页面

  }, {
    key: "toConfirmation",
    value: function toConfirmation() {
      var Tmplist = [];
      var len = this.state.goodsList.length;
      for (var i = 0; i < len; i++) {
        if (this.state.goodsList[i].selected) {
          Tmplist.push(this.state.goodsList[i]);
        }
      }
      if (Tmplist.length < 1) {
        _index2.default.showToast({
          title: '没有商品',
          icon: 'none'
        });
        return;
      }
      _index2.default.setStorage({
        key: 'key',
        data: Tmplist,
        success: function success() {
          _index2.default.navigateTo({
            url: '/pages/jinghuobao/Confirmation'
          });
        }
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;

      var goodsList = this.__state.goodsList;

      var anonymousState__temp = (0, _index.internal_inline_style)({ marginTop: '50px' });
      var anonymousState__temp2 = (0, _index.internal_inline_style)({ height: '100px' });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2
      });
      return this.__state;
    }
  }]);

  return Shop;
}(_index.Component), _class.properties = {}, _class.$$events = ["choose", "delc", "infc", "allSelect", "deleteList", "toConfirmation"], _temp2);
exports.default = Shop;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Shop, true));