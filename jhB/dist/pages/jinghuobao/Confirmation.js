"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addricon = "/pages/jinghuobao/img/addricon.png";

var Confirmation = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Confirmation, _BaseComponent);

  function Confirmation() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Confirmation);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Confirmation.__proto__ || Object.getPrototypeOf(Confirmation)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["addricon", "buylist", "goodsPrice", "sumPrice", "freight", "note", "int", "deduction"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Confirmation, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Confirmation.prototype.__proto__ || Object.getPrototypeOf(Confirmation.prototype), "_constructor", this).call(this, props);
      this.state = {
        buylist: [], //订单列表
        goodsPrice: 0.0, //商品合计价格
        sumPrice: 0.0, //用户付款价格
        freight: 12.00, //运费
        note: '', //备注
        int: 1200, //抵扣积分
        deduction: 0
      };
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var _this2 = this;

      _index2.default.getStorage({
        key: 'key',
        success: function success(res) {
          res.data;
          _this2.setState({
            buylist: res.data
          }, function () {
            console.log(_this2.state.buylist);
            //合计
            var len = _this2.state.buylist.length;
            for (var i = 0; i < len; i++) {
              _this2.state.goodsPrice = _this2.state.goodsPrice + _this2.state.buylist[i].number * _this2.state.buylist[i].price;
              _this2.setState({
                goodsPrice: _this2.state.goodsPrice
              });
            }
            _this2.state.deduction = _this2.state.int / 100;
            _this2.state.sumPrice = _this2.state.goodsPrice - _this2.state.deduction + _this2.state.freight;
          });
        }
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;
      Object.assign(this.__state, {
        addricon: addricon
      });
      return this.__state;
    }
  }]);

  return Confirmation;
}(_index.Component), _class.properties = {}, _class.$$events = [], _temp2);
exports.default = Confirmation;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Confirmation, true));