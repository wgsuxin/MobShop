"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Goods = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Goods, _BaseComponent);

  function Goods() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Goods);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Goods.__proto__ || Object.getPrototypeOf(Goods)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "anonymousState__temp4", "anonymousState__temp5", "anonymousState__temp6", "anonymousState__temp7", "anonymousState__temp8", "xs", "showView", "showViewchoose", "goodsflag", "goodscol", "ind", "shuliang"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Goods, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Goods.prototype.__proto__ || Object.getPrototypeOf(Goods.prototype), "_constructor", this).call(this, props);
      this.state = {
        showView: false,
        showViewchoose: false,
        goodsflag: true,
        goodscol: true,
        xs: ['xs', 's', 'm', 'l', 'xl', 'xxl'],
        ind: [],
        shuliang: 1
      };
      this.handleopen = this.handleopen.bind(this);
      this.handleshow = this.handleshow.bind(this);
      this.handelopen = this.handelopen.bind(this);
      this.handleshowchoose = this.handleshowchoose.bind(this);
      this.inc = this.inc.bind(this);
      this.delc = this.delc.bind(this);
      this.infc = this.infc.bind(this);
      this.backtop = this.backtop.bind(this);
    }
  }, {
    key: "handleopen",
    value: function handleopen() {
      console.log("hhh");
      this.setState({

        showView: !this.state.showView
      });
    }
  }, {
    key: "handleshow",
    value: function handleshow() {
      this.setState({

        showView: !this.state.showView
      });
    }
  }, {
    key: "handelopen",
    value: function handelopen() {
      this.setState({
        showViewchoose: !this.state.showViewchoose
      });
    }
  }, {
    key: "handleshowchoose",
    value: function handleshowchoose() {
      this.setState({
        showViewchoose: !this.state.showViewchoose
      });
    }
  }, {
    key: "inc",
    value: function inc(index) {

      this.setState({
        ind: index
      });
    }
  }, {
    key: "delc",
    value: function delc() {
      if (this.state.shuliang > 1) {
        this.setState({
          shuliang: this.state.shuliang - 1
        });
      }
    }
  }, {
    key: "infc",
    value: function infc() {
      this.setState({
        shuliang: this.state.shuliang + 1
      });
    }
  }, {
    key: "onPageScroll",
    value: function onPageScroll(e) {
      // 获取滚动条当前位置
      var top = e.scrollTop;
      console.log(e.scrollTop);
      if (top > 100) {
        this.setState({
          goodsflag: false,
          goodscol: false
        });
      } else {
        this.setState({
          goodsflag: true,
          goodscol: true
        });
      }
    }
  }, {
    key: "backtop",
    value: function backtop() {
      _index2.default.pageScrollTo({
        scrollTop: 0,
        duration: 300
      });
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {

      this.$router.params.url;
      console.log(this.$router.params.url);
      console.log('preload: ', this.$router.preload.key);
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;

      var xs = this.__state.xs;

      var anonymousState__temp = (0, _index.internal_inline_style)({ backgroundColor: '#F8F8F8' });
      var anonymousState__temp2 = (0, _index.internal_inline_style)({ marginTop: '10px' });
      var anonymousState__temp3 = (0, _index.internal_inline_style)({ backgroundColor: 'white', padding: '10px' });
      var anonymousState__temp4 = (0, _index.internal_inline_style)({ marginTop: '15px' });
      var anonymousState__temp5 = (0, _index.internal_inline_style)({ marginTop: '15px' });
      var anonymousState__temp6 = (0, _index.internal_inline_style)({ marginTop: '15px' });
      var anonymousState__temp7 = (0, _index.internal_inline_style)({ marginTop: '20px' });
      var anonymousState__temp8 = (0, _index.internal_inline_style)({ margin: '10px' });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        anonymousState__temp4: anonymousState__temp4,
        anonymousState__temp5: anonymousState__temp5,
        anonymousState__temp6: anonymousState__temp6,
        anonymousState__temp7: anonymousState__temp7,
        anonymousState__temp8: anonymousState__temp8
      });
      return this.__state;
    }
  }]);

  return Goods;
}(_index.Component), _class.properties = {}, _class.$$events = ["backtop", "handleopen", "handleshow", "handelopen", "inc", "delc", "infc", "handleshowchoose"], _temp2);
exports.default = Goods;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Goods, true));