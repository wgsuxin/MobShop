"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var no = "/pages/jinghuobao/img/noorder.png";
var p1 = "/pages/jinghuobao/img/goods/p1.jpg";
var p2 = "/pages/jinghuobao/img/goods/p2.jpg";
var p3 = "/pages/jinghuobao/img/goods/p3.jpg";
var p4 = "/pages/jinghuobao/img/goods/p4.jpg";
var p5 = "/pages/jinghuobao/img/goods/p5.jpg";
var p6 = "/pages/jinghuobao/img/goods/p6.jpg";

var Orderlist = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Orderlist, _BaseComponent);

  function Orderlist() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Orderlist);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Orderlist.__proto__ || Object.getPrototypeOf(Orderlist)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "loopArray0", "orderType", "list", "no", "headerPosition", "tabarIndex", "headerTop", "typeText", "orderList", "loading"], _this.$$refs = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Orderlist, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Orderlist.prototype.__proto__ || Object.getPrototypeOf(Orderlist.prototype), "_constructor", this).call(this, props);
      this.state = {
        headerPosition: "fixed",
        tabarIndex: 0,
        headerTop: "0px",
        typeText: {
          unpaid: '等待付款',
          back: '等待商家发货',
          unreceived: '商家已发货',
          received: '等待用户评价',
          completed: '交易已完成',
          refunds: '商品退货处理中'
        },
        orderType: ['全部', '待付款', '待发货', '待收货', '待评价', '退换货'],

        orderList: [[{ type: "unpaid", ordersn: 0, goods_id: 0, img: p1, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "unpaid", ordersn: 1, goods_id: 1, img: p2, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "back", ordersn: 1, goods_id: 1, img: p3, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "unreceived", ordersn: 1, goods_id: 1, img: p4, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "received", ordersn: 1, goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "completed", ordersn: 1, goods_id: 1, img: p6, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "refunds", ordersn: 1, goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }], [{ type: "unpaid", ordersn: 0, goods_id: 0, img: p1, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }, { type: "unpaid", ordersn: 1, goods_id: 1, img: p2, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }], [
          //无
        ], [{ type: "unreceived", ordersn: 1, goods_id: 1, img: p4, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }], [{ type: "received", ordersn: 1, goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }], [{ type: "refunds", ordersn: 1, goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168', payment: 168.00, freight: 12.00, spec: '规格:S码', numner: 1 }]],
        list: [],
        loading: true

      };
      this.showType = this.showType.bind(this);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var itemlist = this.state.orderList;
      var that = this;
      setTimeout(function () {
        that.setState({
          loading: false,
          list: itemlist[that.state.tabarIndex]
        });
        console.log(that.state.list);
      }, 1500);
      console.log(this.state.list);
    }
  }, {
    key: "showType",
    value: function showType(tbIndex) {

      var index = tbIndex.currentTarget.dataset.id;
      var itemlists = [].concat(_toConsumableArray(this.state.orderList));
      this.setState({
        tabarIndex: index,
        list: itemlists[index]
      });

      console.log(this.state.list);
    }
  }, {
    key: "_createData",
    value: function _createData() {
      var _this2 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __runloopRef = arguments[2];
      ;

      var orderType = this.__state.orderType;
      var list = this.__state.list;

      var anonymousState__temp = (0, _index.internal_inline_style)({ position: 'headerPosition', top: 'headerTop' });
      var loopArray0 = list.map(function (item, tabindex) {
        item = {
          $original: (0, _index.internal_get_original)(item)
        };
        var $loopState__temp3 = _this2.__state.typeText[item.$original.type];
        return {
          $loopState__temp3: $loopState__temp3,
          $original: item.$original
        };
      });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        loopArray0: loopArray0,
        no: no
      });
      return this.__state;
    }
  }]);

  return Orderlist;
}(_index.Component), _class.properties = {}, _class.$$events = ["showType"], _temp2);
exports.default = Orderlist;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Orderlist, true));