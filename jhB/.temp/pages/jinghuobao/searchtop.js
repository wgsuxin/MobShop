import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Input, Icon } from '@tarojs/components';

import './Classify.css';

class Searchtop extends Component {
  constructor() {
    super(...arguments);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  render() {
    return <View className="classify-view"><Icon className="classify-icon" size="15" type="search" /><Input type="text" className="classify-search" placeholder="请输入搜索关键词" /></View>;
  }
}
export default Searchtop;