import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text } from '@tarojs/components';

import './shopindex.css';
import Buttonfab from "./button";

class Shopindex extends Component {
  constructor() {
    super(...arguments);
  }

  config = {
    navigationBarTitleText: '店铺首页'
  };

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  render() {
    return <View className="shopindex">
        <View className="shopindex-top"></View>
        <View className="shopindex-top-img"></View>
        <View className="shopindex-top-name">店铺名称</View>
        <View className="shopindex-top-title">店铺公告:店铺动态等文字信息</View>
        <View className="shopindex-top-summary">店铺地址:详细地址XXX</View>
        <Text className="shopindex-top-btn">关注</Text>
        <Text className="shopindex-top-btn">分享</Text>
        {/*会员专区*/}
        <View className="shopindex-top-member">
            <View className="shopindex-top-member-title">会员专区</View>
            <View className="shopindex-member-left">
                <View className="shopindex-member-img"></View>
                <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text></View>
            </View>
            <View className="shopindex-member-right">
                <View className="shopindex-member-img"></View>
                <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text></View>
            </View>
        </View>
        <View className="clear"></View>
        {/*新品上架*/}
        <View className="shopindex-top-member">
            <View className="shopindex-top-news-title">会员专区</View>
            <View className="shopindex-member-left">
                <View className="shopindex-member-img"></View>
                <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text></View>
            </View>
            <View className="shopindex-member-right">
                <View className="shopindex-member-img"></View>
                <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text></View>
            </View>
        </View>
        
        <Buttonfab />
        
      </View>;
  }
}
export default Shopindex;