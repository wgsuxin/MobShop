import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text } from '@tarojs/components';
import { AtIcon } from 'taro-ui';
import './button.css';

class Buttonfab extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      showView: true
    };
    this.onChangeShowState = this.onChangeShowState.bind(this);
  }
  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  onChangeShowState() {
    this.setState({
      showView: !this.state.showView
    });
  }
  toindex() {
    Taro.switchTab({
      url: '/pages/jinghuobao/Index'
    });
  }
  tosearch() {
    Taro.navigateTo({
      url: '/pages/jinghuobao/Search'
    });
  }
  toclassify() {
    Taro.switchTab({
      url: '/pages/jinghuobao/Classify'
    });
  }
  toshopcar() {
    Taro.switchTab({
      url: '/pages/jinghuobao/shop'
    });
  }
  tohome() {
    Taro.switchTab({
      url: '/pages/jinghuobao/home'
    });
  }
  render() {
    return <View className="button">
        <View className="buttonfab" onClick={this.onChangeShowState}>...</View>
        <View className="{{showView?'bt-hide':'show'}}">
            <View className="buttonfab-view">
                <View className="button-border" onClick={this.toindex}><AtIcon value="home" size="15" color="#f8f8f8"></AtIcon><Text className="button-icon">首页</Text></View>
                <View className="button-border"><AtIcon value="search" size="15" color="#f8f8f8"></AtIcon><Text className="button-icon" onClick={this.tosearch}>搜索</Text></View>
                <View className="button-border"><AtIcon value="list" size="15" color="#f8f8f8"></AtIcon><Text className="button-icon" onClick={this.toclassify}>分类</Text></View>
                <View className="button-border"><AtIcon value="shopping-cart" size="15" color="#f8f8f8"></AtIcon><Text className="button-icon" onClick={this.toshopcar}>进货车</Text></View>
                <View className="button-border"><AtIcon value="user" size="15" color="#f8f8f8"></AtIcon><Text className="button-icon" onClick={this.tohome}>个人中心</Text></View>
            </View>
            <View className="triangle-down"></View>
        </View>
      </View>;
  }
}
export default Buttonfab;