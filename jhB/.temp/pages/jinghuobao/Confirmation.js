import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View } from '@tarojs/components';
import './Confirmation.css';

import addricon from './img/addricon.png';

class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buylist: [], //订单列表
      goodsPrice: 0.0, //商品合计价格
      sumPrice: 0.0, //用户付款价格
      freight: 12.00, //运费
      note: '', //备注
      int: 1200, //抵扣积分
      deduction: 0
    };
  }

  render() {
    return <View>
                 {/*收货地址*/}
                <View className="addr">
                    <View className="icon">
                        <Image src={addricon} mode="widthFix"></Image>
                    </View>
                    <View className="right">
                        <View className="tel-name">
                            <View className="name">
                                小王子
                            </View>
                            <View className="tel">
                                188****1688
                            </View>
                        </View>
                        <View className="addres">
                            广东省广州市越秀区沙河大街1024-8号
                        </View>
                    </View>
                </View>
                {/*购买商品列表*/} 
                <View className="buy-list">
                
                {this.state.buylist.map((row, index) => <View className="row" key={index}>
                        <View className="goods-info">
                            <View className="img">
                                <Image src={row.img}></Image>
                            </View>
                            <View className="info">
                                <View className="title">{row.name}</View>
                                <View className="spec">选择{row.spec} 数量:{row.number}</View>
                                <View className="price-number">
                                    <View className="price">￥{row.price * row.number}</View>
                                    <View className="number">
                                        
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>)}
              </View>
    
                {/* 提示-备注*/}
                <View className="order">
                    <View className="row">
                        <View className="left">
                            积分 :
                        </View>
                        <View className="right">
                            已扣除{int}积分抵扣{deduction}元
                        </View>
                    </View>
                    <View className="row">
                        <View className="left">
                            备注 :
                        </View>
                        <View className="right">
                            <input placeholder="选填,备注内容" v-model="note" />
                        </View>
                    </View>
                </View>
                {/*明细*/} 
                <View className="detail">
                    <View className="row">
                        <View className="nominal">
                            商品金额
                        </View>
                        <View className="content">
                            ￥{goodsPrice}
                        </View>
                    </View>
                    <View className="row">
                        <View className="nominal">
                            运费
                        </View>
                        <View className="content">
                            ￥+{freight}
                        </View>
                    </View>
                    <View className="row">
                        <View className="nominal">
                            积分抵扣
                        </View>
                        <View className="content">
                            ￥-{deduction}
                        </View>
                    </View>
                </View>
                <View className="blck">
                    
                </View>
                <View className="footer">
                    <View className="settlement">
                        <View className="sum">合计:<View className="money">￥{sumPrice}</View></View>
                        <View className="btn">提交订单</View>
                    </View>
                </View>
            </View>;
  }

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}

}
export default Confirmation;