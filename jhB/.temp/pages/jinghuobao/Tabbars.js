import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text, Image } from '@tarojs/components';
import './tabar.css';

import pyq from './img/share/pyq.png';
import qq from './img/share/qq.png';
import wb from './img/share/wb.png';
import wx from './img/share/wx.png';

class Tabarbottom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stars: true,
      sharps: false
    };
    this.handleshare = this.handleshare.bind(this);
    this.sharp = this.sharp.bind(this);
  }
  handleshare() {
    this.setState({
      stars: !this.state.stars
    });
  }
  sharp() {
    this.setState({
      sharps: !this.state.sharps
    });
  }
  render() {
    return <View className="tabbar fa1">
        <View className="at-rowtabbar">
           <View className="at-row">
             <View className="at-col tabbar-share-bg" onClick={this.sharp}>
                <View>
                  <Text className="fa fa-share-alt fa-lg tabbar-share"></Text>
                </View>
                分享
             </View>
             
             {/*分享的内容*/}
             <View className="share-flag {{sharps?'sharpshow':'sharphide'}}">
                <View className="share-flag-btm">
                  <View style={{ textAlign: 'center' }}>分享</View>
                  <View className="at-row">
                    <View className="at-col-3 at-col-center">
                      <Image className="at-row-img" src={qq} mode="widthFix" />
                      <View>QQ</View>
                    </View>
                    <View className="at-col-3 at-col-center">
                      <Image className="at-row-img" src={pyq} mode="widthFix" />
                      <View>朋友圈</View>
                    </View>
                    <View className="at-col-3 at-col-center">
                      <Image className="at-row-img" src={wb} mode="widthFix" />
                      <View>新浪微博</View>
                    </View>
                    <View className="at-col-3 at-col-center">
                      <Image className="at-row-img" src={wx} mode="widthFix" />
                      <View>微信好友</View>
                    </View>
                  </View>
                  <View onClick={this.sharp} className="sharp-dele">取消</View>
                </View>
             </View>
             
             <View className="at-col">
                 <View>
                  <Text className="fa fa-commenting-o  fa-lg tabbar-share"></Text>
                </View>
                客服
             </View>
             <View onClick={this.handleshare} className="at-col {{stars?'starshow':'starhiden'}}"> <View> 
                  <Text className="fa tabbar-star fa-bell fa-lg "></Text>
                </View>
                收藏
             </View>
             <View onClick={this.handleshare} className="at-col {{stars?'starhiden':'starshow'}}"> <View>
                  <Text className="fa tabbar-star fa-star fa-lg "></Text>
                </View>
                已收藏
             </View>
           </View>
           
          <View className="tabbars-text">
            <Text className="tabbars-shop">加入购物车</Text>
            <Text className="tabbars-get">立即购买</Text>
          </View>
           
        </View>  
       </View>;
  }
}
export default Tabarbottom;