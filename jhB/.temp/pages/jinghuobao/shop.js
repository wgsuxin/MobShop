import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text } from '@tarojs/components';

import './shop.css';
import p1 from './img/goods/p1.jpg';
import p2 from './img/goods/p2.jpg';
import p3 from './img/goods/p3.jpg';
import p4 from './img/goods/p4.jpg';
import p5 from './img/goods/p5.jpg';

class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shuliang: 1,
      sumPrice: '0.00',
      headerPosition: "fixed",
      headerTop: null,
      statusTop: null,
      selectedList: [],
      isAllselected: false,
      goodsList: [{ id: 1, img: p1, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }, { id: 2, img: p2, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }, { id: 3, img: p3, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }, { id: 4, img: p4, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }, { id: 5, img: p5, name: '商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题', spec: '规格:S码', price: 127.5, number: 1, selected: false }],
      theIndex: null,
      oldIndex: null,
      isStop: false
    };
    this.delc = this.delc.bind(this);
    this.infc = this.infc.bind(this);
    this.deleteList = this.deleteList.bind(this);
  }
  config = {
    navigationBarTitleText: '进货车'
  };

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  delc(num, e) {
    console.log(num);
    e.stopPropagation();
    const num1 = num.currentTarget.dataset.num;
    const item = this.state.goodsList.number;
    if (num1 > 1) {
      this.setState({
        item: num1 - 1
      });
    }
  }
  infc() {
    this.setState({
      item: num1 + 1
    });
  }
  //顶部删除商品
  deleteGoods(id) {
    let len = this.goodsList.length;
    for (let i = 0; i < len; i++) {
      if (id == this.goodsList[i].id) {
        this.goodsList.splice(i, 1);
        break;
      }
    }
    //this.sum();
    this.oldIndex = null;
    this.theIndex = null;
  }
  //下部删除商品
  deleteList() {
    let len = this.selectedList.length;
    for (let i = 0; i < len; i++) {
      this.deleteGoods(this.selectedList[i]);
    }
    this.selectedList = [];
    this.isAllselected = this.selectedList.length == this.goodsList.length && this.goodsList.length > 0;
    //this.sum();
  }

  render() {
    const { goodsList } = this.state;
    return <View>
    {/*
         <View className="shop" >
           <View className='shop-view'><AtIcon className='shop-icon' value='shopping-cart' size='70' color='gray'></AtIcon>
           <View className='shop-view-font'>啊欧!!还什么都没有</View>
           <View className='shop-view-bt'>赶紧去选购</View>
           </View>
         </View>
         */}
       <View className="shop-top">
         购物车
       </View>
       <View style={{ marginTop: '50px' }} />
         {goodsList.map((item, index) => <View className="atrow" key={item.id}>
          {/*删除按钮
           <View className="menu" onClick={this.deleteGoods.bind(this,item.id)}>
           	<View className="icon shanchu"><Text className="fa fa-trash-o"></Text></View>
           </View>*/}
           <View className="carrier">
             <View className="checkbox-box">
               <View className="checkbox">
                <View className="on"></View>
               </View>
             </View>
             <View className="flex-row">
                <View className="flex-col-4"><Image className="flex-col-img" src={item.img} mode="widthFix" /></View>
                <View className="flex-col-8">
                  <View className="flex-col-title">
                   {item.name}
                  </View>
                  <View className="flex-col-summary">
                      {item.spec}
                  </View>
                  <View className="flex-col-btm">
                    <View className="flex-col-price">
                         {item.price}
                    </View>
                    <View className="shop-shuliang">
                       
                        <View className="goods-shuliang-right">
                            <Text data-num={item.number} onClick={this.delc} className="fa fa-minus-square fa-lg"></Text>
                            <Text data-num={item.number} className="goods-shuliang-zonghe">{item.number}</Text>
                            <Text onClick={this.infc} className="fa fa-plus-square fa-lg"></Text>
                        </View>
                    </View>
                  </View>
                  
                </View>
              </View> 
            </View>
           </View>)}
         <View style={{ height: '100px' }} />
       <View className="shop-btm">
          <View className="checkbox-box">
            <View className="checkbox">
             <View className="on"></View>
            </View>
            <View className="shop-btm-flex">全选</View>
          </View>
          <View className="delBtn {{selectedList.length>0?'show':'hiden'}}" onClick={this.deleteList}>删除</View>
          <View className="shop-btm-pay">
            合算:<Text>￥:0.00</Text>
            <View className="shop-btm-balace">结算</View>
          </View>
         
       </View>
     </View>;
  }
}
export default Shop;