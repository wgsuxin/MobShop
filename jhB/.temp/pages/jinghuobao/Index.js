import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text, Icon, Swiper, SwiperItem } from '@tarojs/components';
import './indexs.css';
import right from './img/right.png';
import hua from './img/hua.png';
import p1 from './img/goods/p1.jpg';
import p2 from './img/goods/p2.jpg';
import p3 from './img/goods/p3.jpg';
import p4 from './img/goods/p4.jpg';
import p5 from './img/goods/p5.jpg';
import p6 from './img/goods/p6.jpg';
import p7 from './img/goods/p7.jpg';
import p8 from './img/goods/p8.jpg';
import p9 from './img/goods/p9.jpg';
import p10 from './img/goods/p10.jpg';


class Index extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      productList: [{
        goods_id: 0,
        img: p1,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 1,
        img: p2,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 2,
        img: p3,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 3,
        img: p4,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 4,
        img: p5,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 5,
        img: p6,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 6,
        img: p7,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 7,
        img: p8,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 8,
        img: p9,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }, {
        goods_id: 9,
        img: p10,
        name: '商品名称商品名称商品名称商品名称商品名称',
        price: '￥168',
        slogan: '1235人付款'
      }],
      loadingText: '正在加载...'
    };
    this.toGoods = this.toGoods.bind(this);
    this.toprolist = this.toprolist.bind(this);
  }

  config = {
    navigationBarTitleText: '首页'
  };

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  toGoods(goods) {
    const good = goods.currentTarget.dataset.id;
    console.log(good);
    this.$preload('key', 'val');
    Taro.navigateTo({
      url: "/pages/jinghuobao/goods?url=" + good
    });
  }
  toprolist() {
    Taro.navigateTo({
      url: "/pages/jinghuobao/prolist"
    });
  }
  render() {
    return <View>
        <View className="indexsreachviewbg">
          <View className="indexsreachview">
            <Icon className="indexsreachsearch" size="15" type="search" />
            <View className="input-box">
              <input placeholder="默认关键字" type="text" className="indexsreach" />
            
            </View>
            <View className="icon-btn">
              <View className="icon"><Text className="fa fa-microphone"></Text></View>
              <View className="icon"><Text className="fa fa-bell-o"></Text></View>
            </View>
          </View>
        </View>
        <Swiper indicatorColor="#999" indicatorActiveColor="#333" circular indicatorDots autoplay>
          <SwiperItem className="indexswiper">
            <View>1</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>2</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>3</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>4</View>
          </SwiperItem>
        </Swiper>
        <View className="clear"></View>
        <View className="indexcont">
          <View className="indexconti">
            <View className="indexcontimg">潮流女装</View>
            <Text className="indexconttext">潮流女装</Text>
          </View>
          <View className="indexconti">
            <View className="indexcontimg">潮流女装</View>
            <Text className="indexconttext">潮流女装</Text>
          </View>
          <View className="indexconti">
            <View className="indexcontimg">潮流女装</View>
            <Text className="indexconttext">潮流女装</Text>
          </View>
          <View className="indexconti">
            <View className="indexcontimg">潮流女装</View>
            <Text className="indexconttext">潮流女装</Text>
          </View>
        </View>
        <View className="indexactivity">
          <Text className="indexactivitytext">热门活动</Text>
          <View>
            <View className="indexactivityimg"></View>
            <View className="indexactivityimg1">
              <View className="indexactivityimg2"></View>
              <View className="indexactivityimg3"></View>
            </View>
          </View>
          <View className="clear"></View>
        </View>
        <View className="indexshop">
          <Text className="indexshoptext">品牌商店</Text>
          <View className="indexshop-left" onClick={this.toprolist}>
            <View className="indexshop-left-img">照片</View>
            <View className="indexshop-right">
              <Text className="indexshop-right-text">店铺名称</Text>
              <View className="indexshop-right-View">
                <Text className="indexshop-right-text1">店铺简介,如果你不能清楚地表达它,只能说明你不够了解他</Text>
                {<img className="indexshop-right-text2" src={right} />}
              </View>
            </View>
            
          </View>
          <View className="indexshop-left" onClick={this.toprolist}>
            <View className="indexshop-left-img">照片</View>
            <View className="indexshop-right">
              <Text className="indexshop-right-text">店铺名称</Text>
              <View className="indexshop-right-View">
                <Text className="indexshop-right-text1">店铺简介,如果你不能清楚地表达它,只能说明你不够了解他</Text>
                {<img className="indexshop-right-text2" src={right} />}
              </View>
            </View>
            
          </View>
        </View>
        
       {/* <View className='recommend'>
          <Text  className='indexshoptext'>精品推荐</Text>
          <View classNam='recommend-1'>
            <View  className='recommend-shop1'>
              <View  className='recommend-shop'>
                <View className='recommend-left'></View>
                <Text className='recommend-text'>不算低吃UC就是故意呢开就差你的</Text>
              </View>
              <View  style={{marginTop:'10px',}}><Text  className='recommend-text-bot-text1'>￥188</Text><Text className='recommend-text-bot-text2'>￥559</Text></View>
            </View>
            <View  className='recommend-shop2'>
              <View  className='recommend-shop'>
                <View className='recommend-left'></View>
                <Text className='recommend-text'>不算低吃UC就是故意呢开就差你的</Text>
              </View>
              <View style={{marginTop:'10px',}}><Text  className='recommend-text-bot-text1'>￥188</Text><Text className='recommend-text-bot-text2'>￥559</Text></View>
            </View>
            <View className='clear'></View>
          </View>
        </View>*/}
        {/* 商品列表*/}
        
        <View className="goods-list">
        	<View className="title">
        		<Image src={hua}></Image>
        		猜你喜欢
        		<Image src={hua}></Image>
        	</View>
        	<View className="product-list">
          {this.state.productList.map(product => <View className="product" key={product.goods_id} data-id={product} onClick={this.toGoods}>
            	<Image mode="widthFix" src={product.img}></Image>
            	<View className="name">{product.name}</View>
            	<View className="info">
            		<View className="price">{product.price}</View>
            		<View className="slogan">{product.slogan}</View>
            	</View>
            </View>)}
        		
        	</View>
        	<View className="loading-text">{loadingText}</View>
        </View>
        
      </View>;
  }
}
export default Index;