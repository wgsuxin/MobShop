import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text } from '@tarojs/components';
import Searchtop from "./searchtop";

import './shopindex.css';
import './indexs.css';
import './prolist.css';

class Prolist extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      showView: false
    };
    this.handshow = this.handshow.bind(this);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  handshow() {
    console.log("hhh");
    this.setState({

      showView: !this.state.showView
    });
  }
  render() {
    return <View>
        <Searchtop />
        <View className="at-row prolistrow">
          <View className="at-col at-top">人气</View>
          <View className="at-col at-top">销量</View>
          <View className="at-col at-top"><Text className="fa fa-caret-down "></Text>筛选</View>
          <View className="at-col at-col-2 at-top">
              <Text onClick={this.handshow} className="fa fa-th-large {{showView?'pro-hide':'pro-show'}}"></Text>
              <Text onClick={this.handshow} className="fa fa-th-list {{showView?'pro-show':'pro-hide'}}"></Text>
          </View>
        </View>
        {/*两列*/}
        <View className="{{showView?'pro-hide':'pro-show'}}">
           <View className="shopindex-top-member">
                <View className="shopindex-top-member-title"></View>
                <View className="prolist-left">
                    <View className="shopindex-member-img"></View>
                    <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                    <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text><Text className="prolist-text">5件起批</Text></View>
                </View>
                <View className="prolist-right">
                    <View className="shopindex-member-img"></View>
                    <Text className="shopindex-member-title">至尊红酒你数会晤和一年金市场价怒问啊大家看看昵称</Text>
                    <View className="shopindex-member-text"><Text className="shopindex-member-text1">￥188</Text><Text className="shopindex-member-text2">￥559</Text> <Text className="prolist-text">5件起批</Text></View>
                </View>
            </View>
            <View className="clear"></View>
        </View>
        <View className="{{showView?'pro-show':'pro-hide'}}">
            <View className="at-row prolist-row">
               <View className="at-col prolist-img">照片</View>
               <View className="at-col-6">
                <Text className="prolist-title">店铺名称</Text>
                
                <Text className="prolist-summary">店铺简介,如果你不能清楚地表达它,只能说明你不够了解他</Text> 
                <Text className="prolist-xiaoliang">销量:222</Text>
                <Text className="prolist-zy">自营</Text>
                </View>
                <View className="at-col-2 at-icon">
                   <Text className="fa  fa-chevron-right fa-lg"></Text>
                   
               
               </View>
            </View>
        </View>
            
       </View>;
  }
}
export default Prolist;