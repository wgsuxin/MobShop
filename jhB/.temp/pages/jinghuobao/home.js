import Taro from '@tarojs/taro-h5';
import { Component } from "@tarojs/taro-h5";
import Nerv, { Config } from "nervjs";
import { View, Text } from '@tarojs/components';
import { AtIcon } from 'taro-ui';
import './home.css';


class Home extends Component {
  config = {
    navigationBarTitleText: '我的'
  };
  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  constructor() {
    super(...arguments);
    this.state = {
      imgsrc: ""

    };
    this.handimg = this.handimg.bind(this);
    this.toset = this.toset.bind(this);
    this.toewm = this.toewm.bind(this);
    this.toorderlist = this.toorderlist.bind(this);
  }
  onChange(files) {
    this.setState({
      files
    });
  }
  handimg() {
    var that = this;
    Taro.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        const tempFilePaths = res.tempFilePaths;
        that.setState({
          imgsrc: tempFilePaths
        });
      }
    });
  }

  toset() {
    Taro.navigateTo({
      url: '/pages/jinghuobao/setting'
    });
  }
  toewm() {
    Taro.navigateTo({
      url: '/pages/jinghuobao/myewm'
    });
  }
  toorderlist() {
    Taro.navigateTo({
      url: '/pages/jinghuobao/orderlist'
    });
  }
  render() {

    return <View className="home">
      {/*这是我的头像*/}
        <View className="home-top">
            <View onClick={this.handimg}><Image className="home-img" src={this.state.imgsrc} /></View>
            <View className="home-bt">请先登录</View>
        </View>
        <View className="at-row">
            <View style={{ borderLeft: 'none', borderRight: 'none' }} className="at-col col-xian">收藏的商品</View>
            <View tyle={{ borderLeft: 'none' }} className="at-col col-xian">收藏的店铺</View>
            <View style={{ borderLeft: 'none', borderRight: 'none' }} className="at-col col-xian">我的消息</View>
        </View>
        {/*这是我的订单*/}
        <View className="home-order" onClick={this.toorderlist}>
            <View className="home-order-top at-row">
                <View className="at-col-6">
                    <AtIcon value="playlist" size="25" color="black"></AtIcon>
                    <Text style={{ marginLeft: '10px' }}>我的订单</Text>
                </View>
                 <View className="at-col-6 at-aline">
                    <Text>查看全部订单</Text>
                    <AtIcon value="chevron-right" size="25" color="black"></AtIcon>
                </View>
            </View>
            <View className="home-order-bottom at-row">
                <View className="at-col-3 at-col-aline">
                    <AtIcon value="clock" size="25" color="black"></AtIcon>
                    <View>我的订单</View>
                </View>
                 <View className="at-col-3  at-col-aline">
                    <AtIcon value="shopping-cart" size="25" color="black"></AtIcon>
                    <View>代发货</View>
                </View>
                <View className="at-col-3  at-col-aline">
                    <AtIcon value="calendar" size="25" color="black"></AtIcon>
                    <View>待收货</View>
                </View>
                <View className="at-col-3  at-col-aline">
                    <AtIcon value="check-circle" size="25" color="black"></AtIcon>
                    <View>已完成</View>
                </View>
            </View>
        </View>
        {/*个人设置和有话要说*/}
        <View className="home-set" onClick={this.toset}>
            <AtIcon value="settings" size="25" color="black"></AtIcon>
            <Text className="home-set-text">个人设置</Text>
            <AtIcon className="home-set-right" value="chevron-right" size="25" color="black"></AtIcon>
        </View>
        <View className="home-set" onClick={this.toewm}>
            <AtIcon value="mail" size="25" color="black"></AtIcon>
            <Text className="home-set-text">我的二维码</Text>
            <AtIcon className="home-set-right" value="chevron-right" size="25" color="black"></AtIcon>
        </View>
      </View>;
  }
}
export default Home;