import { Component } from "@tarojs/taro-h5";
import Nerv from "nervjs";
import 'taro-ui/dist/style/index.scss';
import './fa4.7.css';

import './app.less';
// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }
import { View, Tabbar, TabbarContainer, TabbarPanel } from '@tarojs/components';
import Taro from '@tarojs/taro-h5';
import { Router, createHistory, mountApis } from '@tarojs/router';
Taro.initPxTransform({
  "designWidth": 750,
  "deviceRatio": {
    "640": 1.17,
    "750": 1,
    "828": 0.905
  }
});

const _taroHistory = createHistory({
  mode: "hash",
  basename: "/",
  customRoutes: {},
  firstPagePath: "/pages/jinghuobao/Index"
});

mountApis(_taroHistory);
class App extends Component {
  state = {
    __tabs: {
      list: [{
        pagePath: "/pages/jinghuobao/Index",
        text: "首页",
        iconPath: require("./pages/jinghuobao/img/star.png"),
        selectedIconPath: require("./pages/jinghuobao/img/stars.png")
      }, {
        pagePath: "/pages/jinghuobao/Classify",
        text: "分类",
        iconPath: require("./pages/jinghuobao/img/star.png"),
        selectedIconPath: require("./pages/jinghuobao/img/stars.png")
      }, {
        pagePath: "/pages/jinghuobao/shop",
        text: "进货车",
        iconPath: require("./pages/jinghuobao/img/star.png"),
        selectedIconPath: require("./pages/jinghuobao/img/stars.png")
      }, {
        pagePath: "/pages/jinghuobao/home",
        text: "我的",
        iconPath: require("./pages/jinghuobao/img/star.png"),
        selectedIconPath: require("./pages/jinghuobao/img/stars.png")
      }],
      color: '#000',
      selectedColor: '#000',
      mode: "hash",
      basename: "/",
      customRoutes: {}
    }
  };

  constructor() {
    super(...arguments);
    /**
     * 指定config的类型声明为: Taro.Config
     *
     * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
     * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
     * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
     */
    Taro._$app = this;
  }
  componentDidMount() {
    this.componentDidShow();
  }
  componentDidShow() {}
  componentDidHide() {}
  componentDidCatchError() {}
  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return <TabbarContainer>

                  <TabbarPanel>
                    
            <Router history={_taroHistory} routes={[{
          path: '/pages/jinghuobao/Index',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_Index" */'./pages/jinghuobao/Index'),
          isIndex: true
        }, {
          path: '/pages/jinghuobao/Classify',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_Classify" */'./pages/jinghuobao/Classify'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/home',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_home" */'./pages/jinghuobao/home'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/Search',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_Search" */'./pages/jinghuobao/Search'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/shoplist',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_shoplist" */'./pages/jinghuobao/shoplist'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/shopindex',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_shopindex" */'./pages/jinghuobao/shopindex'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/goods',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_goods" */'./pages/jinghuobao/goods'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/shop',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_shop" */'./pages/jinghuobao/shop'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/msg',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_msg" */'./pages/jinghuobao/msg'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/setting',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_setting" */'./pages/jinghuobao/setting'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/orderlist',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_orderlist" */'./pages/jinghuobao/orderlist'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/myewm',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_myewm" */'./pages/jinghuobao/myewm'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/Confirmation',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_Confirmation" */'./pages/jinghuobao/Confirmation'),
          isIndex: false
        }, {
          path: '/pages/jinghuobao/prolist',
          componentLoader: () => import( /* webpackChunkName: "jinghuobao_prolist" */'./pages/jinghuobao/prolist'),
          isIndex: false
        }]} customRoutes={{}} />
            
                  </TabbarPanel>

                  <Tabbar conf={this.state.__tabs} homePage="pages/jinghuobao/Index" router={Taro} />

                </TabbarContainer>;
  }
  config = {
    pages: ["/pages/jinghuobao/Index", "/pages/jinghuobao/Classify", "/pages/jinghuobao/home", "/pages/jinghuobao/Search", "/pages/jinghuobao/shoplist", "/pages/jinghuobao/shopindex", "/pages/jinghuobao/goods", "/pages/jinghuobao/shop", "/pages/jinghuobao/msg", "/pages/jinghuobao/setting", "/pages/jinghuobao/orderlist", "/pages/jinghuobao/myewm", "/pages/jinghuobao/Confirmation", "/pages/jinghuobao/prolist"],
    tabBar: { list: [{ pagePath: "/pages/jinghuobao/Index", text: "首页", iconPath: require("./pages/jinghuobao/img/star.png"), selectedIconPath: require("./pages/jinghuobao/img/stars.png") }, { pagePath: "/pages/jinghuobao/Classify", text: "分类", iconPath: require("./pages/jinghuobao/img/star.png"), selectedIconPath: require("./pages/jinghuobao/img/stars.png") }, { pagePath: "/pages/jinghuobao/shop", text: "进货车", iconPath: require("./pages/jinghuobao/img/star.png"), selectedIconPath: require("./pages/jinghuobao/img/stars.png") }, { pagePath: "/pages/jinghuobao/home", text: "我的", iconPath: require("./pages/jinghuobao/img/star.png"), selectedIconPath: require("./pages/jinghuobao/img/stars.png") }], color: '#000', selectedColor: '#000', mode: "hash",
      basename: "/",
      customRoutes: {}
    },
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    }
  };

  componentWillUnmount() {
    this.componentDidHide();
  }

  componentWillMount() {
    Taro.initTabBarApis(this, Taro);
  }

}
Nerv.render(<App />, document.getElementById('app'));