import Taro, { Component, Config } from '@tarojs/taro'
import { AtButton } from 'taro-ui'
import 'taro-ui/dist/style/index.scss'
import './fa4.7.css'
import Index from './pages/jinghuobao/Index'
import Classify from './pages/jinghuobao/Classify'
import Home from './pages/jinghuobao/home'
import Shop from './pages/jinghuobao/shop'
import Search from './pages/jinghuobao/Search'
import Shoplist from './pages/jinghuobao/shoplist'
import Shopindex from './pages/jinghuobao/shopindex'
import Goods from './pages/jinghuobao/goods'
import Msg from './pages/jinghuobao/msg'
import Setting from './pages/jinghuobao/setting'
import Orderlist from './pages/jinghuobao/orderlist'
import Ewm from './pages/jinghuobao/myewm'
import Confirmation  from './pages/jinghuobao/Confirmation'
import Tabarbottom  from './pages/jinghuobao/Tabbars'
import Login from './pages/jinghuobao/login/login'
import './app.less'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    pages: [
       'pages/jinghuobao/Index' 
       'pages/jinghuobao/Classify'
         'pages/jinghuobao/home'
         'pages/jinghuobao/Search'
         'pages/jinghuobao/shoplist'
         'pages/jinghuobao/shopindex'
         'pages/jinghuobao/goods'
         'pages/jinghuobao/shop'
         'pages/jinghuobao/msg'
         'pages/jinghuobao/setting'
         'pages/jinghuobao/orderlist'
         'pages/jinghuobao/myewm'
         'pages/jinghuobao/Confirmation'
         'pages/jinghuobao/prolist' 
         'pages/jinghuobao/login/login'
         'pages/jinghuobao/login/register'
         'pages/jinghuobao/login/resetpasswd'
    ],
     tabBar: {
      list: [{
        pagePath: 'pages/jinghuobao/Index',
        text: "首页",
        iconPath: "pages/jinghuobao/img/star.png",
        selectedIconPath: "pages/jinghuobao/img/stars.png"
      }, {
        pagePath: "pages/jinghuobao/Classify",
        text: "分类",
        iconPath: "pages/jinghuobao/img/star.png",
        selectedIconPath: "pages/jinghuobao/img/stars.png"
      },
       {
        pagePath: "pages/jinghuobao/shop",
        text: "进货车",
        iconPath: "pages/jinghuobao/img/star.png",
        selectedIconPath: "pages/jinghuobao/img/stars.png"
      },
       {
        pagePath: "pages/jinghuobao/home",
        text: "我的",
        iconPath: "pages/jinghuobao/img/star.png",
        selectedIconPath: "pages/jinghuobao/img/stars.png"
      }
      ],
     color:'#000',
     selectedColor: '#000',
    }, 
    
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    }
  }

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Index />
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
