import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem  } from '@tarojs/components'
import Searchtop from './Searchtop'
import { AtIcon } from 'taro-ui'
import right from './img/right.png'
import './shopindex.css'
import './indexs.css'
import './prolist.css'
import Columngoodslist from './public/Columngoodslist'
import Publicgoodslist from './public/goodslist'
import Buttonfab from './button'

class Prolist extends Component {
 constructor() {
    super(...arguments)
    this.state={
        showView: false ,
        tabindex:1,
    }
    this.handshow = this.handshow.bind(this)
    this.ison = this.ison.bind(this)
  }
 


  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
 handshow(){
     console.log("hhh")
     this.setState({
        
       showView: !this.state.showView
     }) 
 }
 ison(id){
     console.log(id)
     const index = id.currentTarget.dataset.id
     this.setState({
         tabindex:index
     })
    
 }
  render () {
    return (
       <View className="boby">
        <Searchtop/>
        <View className='prolistrow'>
          <View onClick={this.ison} data-id={1} className="{{tabindex==1?'on':''}} at-top">人气</View>
          <View onClick={this.ison} data-id={2} className="{{tabindex==2?'on':''}} at-top">销量</View>
          <View onClick={this.ison} data-id={3} className="{{tabindex==3?'on':''}} at-top"><Text className='fa fa-caret-down '></Text>筛选</View>
          <View onClick={this.ison} data-id={4} className="{{tabindex==4?'on':''}} at-top">
              <Text  onClick={this.handshow} className="fa fa-th-large {{showView?'pro-hide':'pro-show'}}" ></Text>
              <Text  onClick={this.handshow} className="fa fa-th-list {{showView?'pro-show':'pro-hide'}}" ></Text>
          </View>
        </View>
        {/*两列*/}
        <View  className="{{showView?'pro-hide':'pro-show'}}">
           <Publicgoodslist / >
        </View>
        <View className="{{showView?'pro-show':'pro-hide'}}" >
            <Columngoodslist />
        </View>
            < Buttonfab />

       </View>
    )
  }
}
export default Prolist;