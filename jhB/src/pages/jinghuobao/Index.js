import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem  } from '@tarojs/components'
import './indexs.css'
import right from './img/right.png'
import hua from './img/hua.png'
import p1 from './img/goods/p1.jpg'
import p2 from './img/goods/p2.jpg'
import p3 from './img/goods/p3.jpg'
import p4 from './img/goods/p4.jpg'
import p5 from './img/goods/p5.jpg'
import p6 from './img/goods/p6.jpg'
import p7 from './img/goods/p7.jpg'
import p8 from './img/goods/p8.jpg'
import p9 from './img/goods/p9.jpg'
import p10 from './img/goods/p10.jpg'
import Prolist from './prolist'
import Searchtop from './Searchtop'
import Publicgoodslist from './public/goodslist'

class Index extends Component {
 constructor() {
    super(...arguments)
    this.state={
      loadingText: '正在加载...'
    }
    
    this.toprolist = this.toprolist.bind(this)
  }
 
  config: Config = {
    navigationBarTitleText: '首页'
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

toprolist(){
  Taro.navigateTo({
    url:"/pages/jinghuobao/prolist"
  })
}
  render () {
    return (
      <View className="body">
        <View className="indexsreachviewbg">
          <Searchtop />
        </View>
        <Swiper
        indicatorColor='#999'
        indicatorActiveColor='#333'
        circular
        indicatorDots
        autoplay>
          <SwiperItem className="indexswiper">
            <View>1</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>2</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>3</View>
          </SwiperItem>
          <SwiperItem className="indexswiper">
            <View>4</View>
          </SwiperItem>
        </Swiper>
        <View className='clear'></View>
        <View className='indexcont'>
          <View  className='indexconti'>
            <View className='indexcontimg'>潮流女装</View>
            <Text className='indexconttext'>潮流女装</Text>
          </View>
          <View  className='indexconti'>
            <View className='indexcontimg'>潮流女装</View>
            <Text className='indexconttext'>潮流女装</Text>
          </View>
          <View  className='indexconti'>
            <View className='indexcontimg'>潮流女装</View>
            <Text className='indexconttext'>潮流女装</Text>
          </View>
          <View  className='indexconti'>
            <View className='indexcontimg'>潮流女装</View>
            <Text className='indexconttext'>潮流女装</Text>
          </View>
        </View>
        <View className='indexactivity'>
          <Text className='indexactivitytext'>热门活动</Text>
          <View>
            <View  className='indexactivityimg'></View>
            <View className='indexactivityimg1'>
              <View  className='indexactivityimg2'></View>
              <View  className='indexactivityimg3'></View>
            </View>
          </View>
          <View className='clear'></View>
        </View>
        <View className='indexshop'>
          <Text  className='indexshoptext'>品牌商店</Text>
          <View className='indexshop-left'  onClick={this.toprolist}>
            <View  className='indexshop-left-img'>照片</View>
            <View className='indexshop-right'>
              <Text  className='indexshop-right-text'>店铺名称</Text>
              <View  className='indexshop-right-View'>
                <Text className='indexshop-right-text1'>店铺简介,如果你不能清楚地表达它,只能说明你不够了解他</Text>
                {process.env.TARO_ENV === 'weapp' ?<Image  className='indexshop-right-text2' src={right} /> : <img  className='indexshop-right-text2' src={right} />}
              </View>
            </View>
            
          </View>
          <View className='indexshop-left' onClick={this.toprolist}>
            <View  className='indexshop-left-img'>照片</View>
            <View className='indexshop-right'>
              <Text  className='indexshop-right-text'>店铺名称</Text>
              <View  className='indexshop-right-View'>
                <Text className='indexshop-right-text1'>店铺简介,如果你不能清楚地表达它,只能说明你不够了解他</Text>
                {process.env.TARO_ENV === 'weapp' ?<Image  className='indexshop-right-text2' src={right} /> : <img  className='indexshop-right-text2' src={right} />}
              </View>
            </View>
            
          </View>
        </View>
        {/* 商品列表*/}
        
        <View className="goods-list">
        	<View className="title">
        		<Image src={hua}></Image>
        		猜你喜欢
        		<Image src={hua}></Image>
        	</View>
        	<Publicgoodslist />
        	<View className="loading-text">{ loadingText }</View>
        </View>
        
      </View>
    )
  }
}
export default Index;