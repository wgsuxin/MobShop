import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem  } from '@tarojs/components'
import right from './img/right.png'
import './searchtop.css'
import '../../fa4.7.css'

class Searchtop extends Component {
 constructor() {
    super(...arguments)
    this.state={
      
    }
    this.tomsg = this.tomsg.bind()
    
  }
 


  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  tomsg(){
    Taro.navigateTo({
      url:'/pages/jinghuobao/msg'
    })
  }
  render () {
    return (
       <View className="indexsreachview">
            <Icon className="indexsreachsearch" size='15' type='search' />
            <View className="input-box">
              <input
                placeholder="默认关键字"
                type="text" 
                className="indexsreach" 
              />
            
            </View>
            <View className="icon-btn">
              <View className="icon"><Text className="fa fa-microphone"></Text></View>
              <View className="icon" onClick={this.tomsg}><Text className="fa fa-bell-o"></Text></View>
            </View>
          
       </View>
    )
  }
}
export default Searchtop;