import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem} from '@tarojs/components'
import Tabarbottom from './Tabbars'
import { AtAvatar } from 'taro-ui'
import './goods.css'
class Goods extends Component{
 constructor(props) {
    super(props)
    this.state={
        showView: false ,
        showViewchoose:false,
        goodsflag:true,
        goodscol:true,
        xs:['xs','s','m','l','xl','xxl'],
        ind:[],
        shuliang:1,
    }
    this.handleopen = this.handleopen.bind(this)
    this.handleshow = this.handleshow.bind(this)
    this.handelopen = this.handelopen.bind(this)
    this.handleshowchoose = this.handleshowchoose.bind(this)
    this.inc=this.inc.bind(this)
    this.delc=this.delc.bind(this)
    this.infc=this.infc.bind(this)
    this.backtop=this.backtop.bind(this)
  }
   handleopen(){
      console.log("hhh")
      this.setState({
         
        showView: !this.state.showView
      }) 
  }
  handleshow(){
      this.setState({
         
        showView: !this.state.showView
      }) 
  }
  handelopen(){
      this.setState({
          showViewchoose: !this.state.showViewchoose
      })
  }
  handleshowchoose(){
       this.setState({
          showViewchoose: !this.state.showViewchoose
      })
  }
  inc(index){
      
      this.setState({
          ind:index
      })
  }
  delc(){
      if(this.state.shuliang>1){
      this.setState({
          shuliang:this.state.shuliang-1
      })
  }
  }
  infc(){
      this.setState({
          shuliang:this.state.shuliang+1
      })
  }

   onPageScroll(e){ // 获取滚动条当前位置
    const top = e.scrollTop
    console.log(e.scrollTop)
    if(top>100){
        this.setState({
            goodsflag:false,
            goodscol:false
        })
    }else{
        this.setState({
            goodsflag:true,
            goodscol:true
        })
    }
}
  backtop(){
      Taro.pageScrollTo({
          scrollTop: 0,
      duration: 300
      })
  }
  
  
  componentWillMount() {
      
      this.$router.params.url
      console.log(this.$router.params.url)
      console.log('preload: ', this.$router.preload.key)
      
  }
      
  
  
  render () {
      
      
      
      const xs=this.state.xs
       
    return (
    <View style={{backgroundColor: '#F8F8F8',}}>
      <View className="at-row goods-flag {{goodsflag?'':'goodsflag-bg'}}">
       <View className='at-col'></View>
       <View className="at-col {{goodscol?'pro-hide':'pro-show'}}" style={{marginTop:'10px'}}>
          <Text className="goods-flag-text" onClick={this.backtop}>主图</Text>
          <Text className="goods-flag-text">评价</Text>
          <Text className="goods-flag-text">详情</Text>
       </View>
       <View className='at-col'>
          <Text className="fa  fa-bell {{goodsflag?'fa-bg':'fa-bbg'}}"></Text>
          <Text className="fa fa-shopping-cart {{goodsflag?'fa-bg':'fa-bbg'}}"></Text>
       </View> 
      </View>  
      <Swiper
        className='test-h'
        indicatorColor='#999'
        indicatorActiveColor='#333'
        circular
        indicatorDots
        autoplay>
        <SwiperItem>
          <View className='demo-text-1'>< Image src="https://s2.ax1x.com/2019/03/28/AdOfUJ.jpg" mode="widthFix" /></View>
        </SwiperItem>
        <SwiperItem>
          <View className='demo-text-2'>< Image src="https://s2.ax1x.com/2019/03/28/AdOWE4.jpg" mode="widthFix" /></View>
        </SwiperItem>
        <SwiperItem>
          <View className='demo-text-3'>< Image src="https://s2.ax1x.com/2019/03/28/AdO2bF.jpg" mode="widthFix" /></View>
        </SwiperItem>
      </Swiper>
      <View style={{backgroundColor: 'white',padding:'10px'}}>
          <Text className='goods-title'>￥1299</Text>
          <Text className='goods-summary'>商品摘要</Text>
      </View>
      <View  className='goods-service-bg' onClick={this.handleopen} >
        <Text className='goods-service'>服务</Text>
        <Text className='goods-service-cont'>正品保障 急速退款 7天退换</Text>
        <Text className='fa fa-chevron-right service-icon'></Text>
        <View  className="{{showView?'pro-show':'pro-hide'}}">
            <View className='goods-service-flag'>
                <View className='goods-service-flag-bg'>
                    <View style={{marginTop:'15px'}}>正品保障</View>
                    <View className='goods-servies-flag-text'>此商品官方保障是正品</View>
                    <View style={{marginTop:'15px'}}>急速退款</View>
                    <View className='goods-servies-flag-text'>此商品享受退货急速退款服务</View>
                    <View style={{marginTop:'15px'}}>7天退换,此商品享受7天无理由退货</View>
                    <View className='goods-servies-flag-text'>此商品官方保障是正品</View>
                    <View className='goods-servies-flag-bt' onClick={this.handleshow}>完成</View>
                </View>
            </View>
        </View>
      
      
      </View>
       
       <View className='at-row goods-choose-bg' onClick={this.handelopen}>
          <View className='at-col-1 goods-choose-text'>选择</View>
          <View className='at-col'>
            <View className='goods-choose'>选择规格</View>
            <View className='goods-choose-view'>
             {
                xs.map((item,index)=>
                  <Text onClick={this.inc.bind(this,index)}  className="goods-choose-xs {{index==ind?'goods-border':'goods-border-none'}}">{item}</Text>
                )
               
               }
               
            </View>
          </View>
          <View className='at-col-2 choose-icon'>
            <Text className='fa fa-chevron-right fa-lg'></Text>
          </View>
       </View>
        <View  className="{{showViewchoose?'choose-show':'choose-hide'}}">
            <View className='goods-service-flag'>
                <View className='goods-service-flag-bg'>
                    <View className='goods-choose'>选择规格</View>
                    <View className='goods-choose-view'>
                       {
                           xs.map((item,index)=>
                             <Text onClick={this.inc.bind(this,index)}  className="goods-choose-xs {{index==ind?'goods-border':'goods-border-none'}}">{item}</Text>
                           )
                          
                          }
                       <View className='goods-shuliang'>
                           <Text>数量</Text>
                           <View className='goods-shuliang-right'>
                               <Text  onClick={this.delc} className='fa fa-minus-square fa-lg'></Text>
                               <Text className='goods-shuliang-zonghe'>{this.state.shuliang}</Text>
                               <Text onClick={this.infc}  className='fa fa-plus-square fa-lg'></Text>
                           </View>
                       </View>
                    </View>
                    <View className='goods-servies-flag-bt' onClick={this.handleshowchoose}>完成</View>
                </View>
            </View>
        </View>
        
        {/*商品评价*/}
        <View className='goods-Reviews'>
            <View className='at-row'>
              <View >商品评价(8188)</View>
              <View className='at-col goods-Reviews-right'>查看全部评论 <Text className='fa fa-angle-right'></Text></View>
            </View>
            <View className='at-row' style={{marginTop:'20px'}}>
                <AtAvatar className='AtAvatar at-col' circle size='small' image='https://s2.ax1x.com/2019/03/28/AdOWE4.jpg'></AtAvatar>
                <Text className='at-col goods-Reviews-ren'>人的昵称</Text>
            </View>
        </View>
        {/*商品详情*/}
        <View className='goods-Detail'>—————<Text style={{margin:'10px'}}>商品详情</Text>—————</View>
        <Image mode="widthFix" src="https://s2.ax1x.com/2019/03/28/AdOogx.jpg" />
        <Image mode="widthFix" src="https://s2.ax1x.com/2019/03/28/AdOHKK.jpg" />
        <Image mode="widthFix" src="https://s2.ax1x.com/2019/03/28/AdOTv6.jpg" />
        
        <Tabarbottom />
         
      </View>
    
    )
  }
}
export default Goods ;
