import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input , Image , Button } from '@tarojs/components'
import './register.css'

class Resetpasswd extends Component {
    constructor(props){
        super(props)
        this.state={
           phoneNumber:"",
           code:'',
           passwd:"",
           getCodeText:'获取验证码',
           getCodeBtnColor:"#ffffff",
           getCodeisWaiting:false
        }
        
        this.doReset = this.doReset.bind(this)
    }
    doReset(){
        
    }
    render(){
        return(
        <View className="bg">
        			<View className="form">
        				<View className="username">
        					<View className="get-code" style="{'color':getCodeBtnColor}">{this.state.getCodeText}</View>
        					<Input placeholder="请输入手机号"/>
        				</View>
        				<View className="code">
        					<Input placeholder="请输入验证码"/>
        				</View>
        				<View className="password">
        					<Input placeholder="请输入密码" />
        				</View>
        				<View className="btn" onClick={this.doReset}>重置密码</View>
        		
        			</View>
        			
        </View>
        )
    }
}
export default Resetpasswd;