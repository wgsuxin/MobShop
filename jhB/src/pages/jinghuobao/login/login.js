import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input , Image , Button } from '@tarojs/components'
import md5 from "md5";
import './login.css'
import head from '../img/login/head.png'
import head1 from '../img/logins/head.png'
import icon_user from '../img/login/icon_user.png'
import icon_user1 from '../img/logins/icon_user.png'
import icon_del from '../img/login/icon_del.png'
import icon_del1 from '../img/logins/icon_del.png'
import icon_pwd from '../img/login/icon_pwd.png'
import icon_pwd1 from '../img/logins/icon_pwd.png'
import icon_pwd_switch from '../img/login/icon_pwd_switch.png'
import icon_pwd_switch1 from '../img/logins/icon_pwd_switch.png'
import qq from '../img/login/qq.png'
import qq1 from '../img/logins/qq.png'
import wechat from '../img/login/wechat.png'
import wechat1 from '../img/logins/wechat.png'
import weibo from '../img/login/weibo.png'
import weibo1 from '../img/logins/weibo.png'
import Register from './register'

class Login extends Component{
   constructor (props){
       super(props)
       const isUni = typeof(uni) !== 'undefined'
       this.state={
           username:'',
           userpwd: '',
           pwdType: 'password',
           imgInfo: {
            head: isUni ? head : head1,
            icon_user: isUni ? icon_user : icon_user1,
            icon_del: isUni ? icon_del: icon_del1,
            icon_pwd: isUni ? icon_pwd :icon_pwd1,
            icon_pwd_switch: isUni ? icon_pwd_switch : icon_pwd_switch1,
            qq: isUni ? qq : qq1,
            wechat: isUni ? wechat :wechat1,
            weibo: isUni ? weibo :weibo1,
            },
           showProvider:{
           	weixin:false,
           	qq:false,
           	sinaweibo:false,
           	xiaomi:false
           }
       }
       this.findPwd = this.findPwd.bind(this)
       this.goReg = this.goReg.bind(this)
       this.login = this.login.bind(this)
       this.switchPwd = this.switchPwd.bind(this)
       this.delUser = this.delUser.bind(this)
       this.handelvalue = this.handelvalue.bind(this)
       this.handelpwd = this.handelpwd.bind(this)
   }
   findPwd(){
       Taro.navigateTo({
           url:'/pages/jinghuobao/login/resetpasswd'
       })
   }
   goReg(){
       Taro.navigateTo({
           url:'/pages/jinghuobao/login/register'
       })
   }
   handelvalue(e){
       const uservalue=e.target.value
       this.setState({
           username:uservalue
       })
   }
   handelpwd(e){
        const handelpwd=e.target.value
       this.setState({
           userpwd:handelpwd
       })
   }
   login(){
       const username = this.state.username
       const userpwd = this.state.userpwd
       if(!(/^1(3|4|5|6|7|8|9)\d{9}$/.test(username))){ 
       	Taro.showToast({title: '请填写正确手机号码',icon:"none"});
       	return false; 
       }
       Taro.showLoading({
       	title: '提交中...'
       })
       setTimeout(()=>{
       	let md5PW = md5(this.state.userpwd)
       	Taro.getStorage({
       		key: 'UserList',
       		success: (res)=>{
                console.log(res)
       			for(let i in res.data){
       				let row = res.data[i];
                    console.log(row)
       				if(row.username==this.state.phoneNumber){
       					Taro.hideLoading()
       					//比对密码
       					if(md5PW == res.data[i].userpwd){
       						Taro.showToast({title: '登录成功',icon:"success"});
       					}else{
       						Taro.showToast({title: '账号或密码不正确',icon:"none"});
       					}
       				}
       			}
       		},
       		fail:(e)=>{
       			Taro.hideLoading()
       			Taro.showToast({title: '手机号码未注册',icon:"none"});
       		}
       	});
       },1000)
   }
   switchPwd(){
       
   }
   delUser(){
       this.setState({
           username:''
       })
   }
   thirdLogin(provider){
      
       	Taro.showLoading();
       	//第三方登录
       	Taro.login({
       		provider: provider,
       		success: (res)=>{
       			console.log("success: "+JSON.stringify(res));
       			//案例直接获取用户信息，一般不是在APP端直接获取用户信息，比如微信，获取一个code，传递给后端，后端再去请求微信服务器获取用户信息
       			Taro.getUserInfo({
       				provider: provider,
       				success: (res)=>{
       					console.log('用户信息：' + JSON.stringify(res.userInfo));
       					Taro.setStorage({
       						key: 'UserInfo',
       						data: {
       							username:res.userInfo.nickName,
       							face:res.userInfo.avatarUrl,
       							signature:'个性签名',
       							integral:0,
       							balance:0,
       							envelope:0
       						},
       						success: function () {
       							Taro.hideLoading()
       							Taro.showToast({title: '登录成功',icon:"success"});
       							setTimeout(function(){
       								Taro.navigateBack();
       							},300)
       						}
       					});
       				}
       			});
       		},
       		fail:(e)=>{
       			console.log("fail: "+JSON.stringify(e));
       		}
       	});
      
   }
    render(){
        return(
         
            <View className="page_login">
            	{/* 头部logo*/}
            	<View className="head">
            		<View className="head_bg">
            			<View className="head_inner_bg">
            				<Image mode='widthFix' style={{width: '55px',height: '65px',}} src={this.state.imgInfo.head} className="head_logo" />
            			</View>
            		</View>
            	</View>
            	{/* 登录form*/}
            	<View className="login_form">
            		<View className="Input">
            			<View className="img">
            				<Image style={{width:'20px',height: '20px',}} src={this.state.imgInfo.icon_user} />
            			</View>
            			<Input type="Text" value={this.state.username} onChange={this.handelvalue} placeholder="请输入用户账号"/>
            			<View className="img">
            				<Image onClick={this.delUser} className="img_del" src={this.state.imgInfo.icon_del} />
            			</View>
            		</View>
            		<View className="line" />
            		<View className="Input">
            			<View className="img">
            				<Image style={{width:'20px',height: '20px',}} src={this.state.imgInfo.icon_pwd} />
            			</View>
            			<Input type="password" value={this.state.userpwd} onChange={this.handelpwd} placeholder="请输入密码" />
            			<View className="img" onClick={this.switchPwd}>
            				<Image className="img_pwd_switch" src={this.state.imgInfo.icon_pwd_switch} />
            			</View>
            		</View>
            	</View>
            	{/* 登录提交*/}
            	<Button className="submit" onClick={this.login}>登录</Button>
            	<View className="opts">
            		<Text onClick={this.goReg} className="Text">立即注册</Text>
            		<Text onClick={this.findPwd} className="Text">忘记密码？</Text>
            	</View>
            	<View className="quick_login_line">
            		
            		<Text className="Text">快速登录</Text>
            	
            	</View>
            	<View className="quick_login_list">
            		<Image onClick={this.thirdLogin.bind(this,'qq')} className="item" src={this.state.imgInfo.qq}/>
            		<Image onClick={this.thirdLogin.bind(this,'wechat')} className="item" src={this.state.imgInfo.wechat} />
            		<Image onClick={this.thirdLogin.bind(this,'weibo')} className="item" src={this.state.imgInfo.weibo}/>
            	</View>
            </View>
        
        )
    }
}


export default Login;
