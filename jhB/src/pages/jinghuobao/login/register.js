import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input , Image , Button } from '@tarojs/components'
import './register.css'
import md5 from "md5";

class Register extends Component {
    constructor(props){
        super(props)
        this.state={
            phoneNumber:"",
            code:'',
            passwd:"",
            getCodeText:'获取验证码',
            getCodeBtnColor:"#ffffff",
            getCodeisWaiting:false
        }
        this.toLogin = this.toLogin.bind(this)
        this.doReg = this.doReg.bind(this)
        this.getCode = this.getCode.bind(this)
        this.handphn = this.handphn.bind(this)
        this.setTimer = this.setTimer.bind(this)
        this.Timer = this.Timer.bind(this)
    }
    Timer(){
        
    }
    toLogin(){
        
    }
    doReg(){
         Taro.navigateTo({
            url:'/pages/jinghuobao/login/login'
        })
    }
    handphn(e){
       const valuephn = e.target.value 
       this.setState({
           phoneNumber:valuephn
       })
    }
    getCode(){
        Taro.hideKeyboard()
        if(this.getCodeisWaiting){
        	return ;
        }
        if(!(/^1[345789]\d{9}$/.test(this.state.phoneNumber))){ 
        	Taro.showToast({title: '请填写正确手机号码',icon:"none"});
        	return false; 
        } 
        
        this.setState({
            getCodeText:"发送中...",
            getCodeisWaiting:true,
            getCodeBtnColor:"rgba(255,255,255,0.5)"
        })
       
        //示例用定时器模拟请求效果
        setTimeout(()=>{
        	Taro.showToast({title: '验证码已发送',icon:"none"});
        	//示例默认1234，生产中请删除这一句。
        	this.code=1234;
        	this.setTimer();
        },1000)
    }
    setTimer(){
        let holdTime = 60;
        this.setState({
           getCodeText: "重新获取(60)"
        })
      
        this.Timer = setInterval(()=>{
        	if(holdTime<=0){
                 this.setState({
                    getCodeText:"获取验证码",
                    getCodeisWaiting:false,
                    getCodeBtnColor:"#ffffff"
                })
        		
        		clearInterval(this.Timer);
        		return ;
        	}
            this.setState({
                getCodeText:"重新获取("+holdTime+")"
            })
        	holdTime--;
        	
        },1000)
    }
    render(){
        return(
        <View className="bg">
        		<View className="form">
        			<View className="username">
        				<View className="get-code" style={{'color':'getCodeBtnColor'}} onClick={this.getCode} >{this.state.getCodeText}</View>
        				<Input placeholder="请输入手机号" value={this.state.phoneNumber} onChange={this.handphn}/>
        			</View>
        			<View className="code">
        				<Input placeholder="请输入验证码"  />
        			</View>
        			<View className="password">
        				<Input placeholder="请输入密码"/>
        			</View>
        			<View className="btn" onClick={this.doReg}>立即注册</View>
        			<View className="res">
        				<View onClick={this.toLogin}>已有账号立即登录</View>
        			</View>
        		</View>
        		
        	</View>
        )
    }
}
export default Register;