import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon } from '@tarojs/components'
import './setting.css'
import face from './img/face.jpg'
class Setting extends Component{
    constructor(props){
        super(props)
        this.state={
            
        }
    }
     render(){
         return(
         <View>
            <View className="content">
            	<View className="list">
            		<View className="row">
            			<View className="title">头像</View>
            			<View className="right"><View className="tis">
            			<Image src={face} mode="widthFix"></Image>
            			</View><Text className="fa fa-angle-right icon"></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">昵称</View>
            			<View className="right"><View className="tis">大黑哥</View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">个性签名</View>
            			<View className="right"><View className="tis">这人太懒了，什么都不写</View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">收货地址</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">账户安全</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            	</View>
            	<View className="list">
            		<View className="row">
            			<View className="title">通知提醒</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">支付设置</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">通用</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            	</View>
            	<View className="list">
            		<View className="row">
            			<View className="title">版本升级</View>
            			<View className="right"><View className="tis">v1.0.1</View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">清除缓存</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon "></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">问题反馈</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon"></Text></View>
            		</View>
            		<View className="row">
            			<View className="title">关于商城</View>
            			<View className="right"><View className="tis"></View><Text className="fa fa-angle-right icon"></Text></View>
            		</View>
            	</View>
            </View>
         
         </View>
         
         )
     }
    
    
}
export default Setting;