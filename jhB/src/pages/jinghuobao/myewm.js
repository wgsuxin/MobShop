import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon } from '@tarojs/components'
import './myewm.css'
import qr from './img/qr.png';
import qrlogo from './img/qrlogo.png';

class Ewm extends Component{
    constructor(props){
        super(props)
        this.state={
            
        }
    }
    
    render(){
        return(
        <View>
        	<View className="block">
        		
        	</View>
        	<View className="QR">
        		<Image src={qr}></Image>
        	</View>
            <View className='btm'>
                <View className="title">
                    扫描二维码，加我好友
                </View>
                
                <View className="logo">
                    <Image mode="widthFix" src={qrlogo}></Image>
                </View>
            </View>
        </View>
        )
    }
    
}
export default Ewm;