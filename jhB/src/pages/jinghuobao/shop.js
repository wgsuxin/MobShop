import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon } from '@tarojs/components'
import { AtIcon } from 'taro-ui'
import './shop.css'
import p1 from './img/goods/p1.jpg'
import p2 from './img/goods/p2.jpg'
import p3 from './img/goods/p3.jpg'
import p4 from './img/goods/p4.jpg'
import p5 from './img/goods/p5.jpg'

class Shop extends Component {
  constructor(props){
    super(props)
    this.state={
      
      shuliang:1,
      sumPrice:'0.00',
      headerPosition:"fixed",
      headerTop:null,
      statusTop:null,
      selectedList:[],
      isAllselected:false,
      goodsList:[
        
      	{id:1,img:p1,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1,selected:false},
      	{id:2,img:p2,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127,number:1,selected:false},
      	{id:3,img:p3,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:128.5,number:1,selected:false},
      	{id:4,img:p4,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:129.5,number:1,selected:false},
      	{id:5,img:p5,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:126.5,number:1,selected:false}
      ],
      theIndex:null,
      oldIndex:null,
      isStop:false
    }
    this.delc=this.delc.bind(this)
    this.infc=this.infc.bind(this)
    this.deleteList = this.deleteList.bind(this)
    this.choose = this.choose.bind(this)
    this.allSelect = this.allSelect.bind(this)
    this.toConfirmation = this.toConfirmation.bind(this)
  }
    config: Config = {
      navigationBarTitleText: '进货车'
    }
    
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

choose(num){
   const index = num.currentTarget.dataset.index
   const itemselected=[...this.state.goodsList]
   itemselected[index].selected=!itemselected[index].selected
   this.setState({
     itemselected 
   },()=>{
   }
   )
   console.log("itemselected")
   let i = this.state.selectedList.indexOf(this.state.goodsList[index].id)
   
   
   if(i>-1){
     let itemlist = this.state.selectedList;
     itemlist.splice(i, 1)
     this.setState({
     selectedList:itemlist
     },()=>{
        console.log("this.state.selectedList.length")
        if(this.state.selectedList.length === this.state.goodsList.length){
          this.setState({
            isAllselected:!this.state.isAllselected
          })
          console.log(this.state.isAllselected)
        }else{
          this.setState({
            isAllselected:false,
          })
          
          console.log(this.state.goodsList.length)
        }
   })
   }else{
   this.setState({
     selectedList:this.state.selectedList.concat(this.state.goodsList[index].id)
   },()=>{
     if(this.state.selectedList.length === this.state.goodsList.length){
       this.setState({
         isAllselected:!this.state.isAllselected
       })
     }else{
       this.setState({
         isAllselected:false,
       })
     }
   })
			} 
      
    this.sum();
 }
//全选
allSelect(){
  let len = this.state.goodsList.length;
  let arr = [];
  for(let i=0;i<len;i++){
    let itemgoodsList = [...this.state.goodsList]
    itemgoodsList[i].selected = this.state.isAllselected? false : true
   console.log(this.state.isAllselected)
  this.setState({
    itemgoodsList
  },()=>{
    console.log(itemgoodsList)
  })
  	arr.push(this.state.goodsList[i].id);
  }
  let itemselectedList=this.state.selectedList
  itemselectedList = this.state.isAllselected?[]:arr;
  this.setState({
    itemselectedList
  })
  this.setState({
    isAllselected : this.state.isAllselected||this.state.goodsList.length==0?false : true
  })
  this.sum();
  }
  
  
delc(num,e){
  
   const index = num.currentTarget.dataset.index
   const item = [...this.state.goodsList]
      if(item[index].number>1){
        item[index].number=item[index].number-1
      this.setState({
          item
      })
  }
  this.sum();
  }
  infc(num){
    const index = num.currentTarget.dataset.index
     const item = [...this.state.goodsList]
     item[index].number=item[index].number+1
        this.setState({
            item
        })
    
     this.sum();
  }
  //顶部删除商品
  deleteGoods(id){
  	let len = this.state.goodsList.length;
  	for(let i=0;i<len;i++){
  		if(id==this.state.goodsList[i].id){
  			this.state.goodsList.splice(i, 1);
  			break;
  		}
  	}
  	this.sum();
  	this.state.oldIndex = null;
  	this.state.theIndex = null;
  }
  //下部删除商品
  deleteList(){
  	let len = this.state.selectedList.length;
  	for(let i=0;i<len;i++){
  		this.deleteGoods(this.state.selectedList[i]);
  	}
  	this.state.selectedList = [];
  	this.state.isAllselected = this.state.selectedList.length == this.state.goodsList.length && this.state.goodsList.length>0;
  	this.sum();
  }
  // 合计
  sum(){
    this.state.sumPrice = 0
  	let len = this.state.goodsList.length;
  	for(let i=0;i<len;i++){
  		if(this.state.goodsList[i].selected) {
         this.state.sumPrice = this.state.sumPrice + (this.state.goodsList[i].number*this.state.goodsList[i].price)
        this.setState({
          sumPrice : this.state.sumPrice
        })
  		}
  	}
    const sumPrice = this.state.sumPrice
    sumPrice.toFixed(2)
    this.setState({
      sumPrice:sumPrice
    })
  }
  //跳转确认订单结算页面
  toConfirmation(){
    let Tmplist = []
    let len = this.state.goodsList.length
    for(let i=0;i<len;i++){
      if(this.state.goodsList[i].selected){
        Tmplist.push(this.state.goodsList[i])
      }
    }
    if(Tmplist.length<1){
      Taro.showToast({
          title: '没有商品',
          icon: 'none',
      })
      return ;
    }
    Taro.setStorage({
      key: 'key',
      data: Tmplist,
      success:()=>{
        Taro.navigateTo({
          url:'/pages/jinghuobao/Confirmation'
        })
      } 
    })
    
    
  }
  render () {
      const {goodsList}=this.state
    return (
    <View>
    {/*
      <View className="shop" >
        <View className='shop-view'><AtIcon className='shop-icon' value='shopping-cart' size='70' color='gray'></AtIcon>
        <View className='shop-view-font'>啊欧!!还什么都没有</View>
        <View className='shop-view-bt'>赶紧去选购</View>
        </View>
      </View>
      */}
       <View className='shop-top'>
         购物车
       </View>
       <View style={{marginTop:'50px'}} />
         {
           
           goodsList.map((item,index)=>
          <View className="atrow" key={item.id}>
          {/*删除按钮
           <View className="menu" onClick={this.deleteGoods.bind(this,item.id)}>
           	<View className="icon shanchu"><Text className="fa fa-trash-o"></Text></View>
           </View>*/}
           <View className="carrier">
             <View className="checkbox-box" data-index={index} data-sele={item.selected} onClick={this.choose}>
               <View className="checkbox">
                <View className="{{item.selected?'on':''}}"></View>
               </View>
             </View>
             <View className='flex-row'>
                <View className='flex-col-4'><Image className='flex-col-img' src={item.img} mode='widthFix'/></View>
                <View className='flex-col-8'>
                  <View className='flex-col-title'>
                   {item.name}
                  </View>
                  <View className='flex-col-summary'>
                      {item.spec}
                  </View>
                  <View className='flex-col-btm'>
                    <View className='flex-col-price'>
                         {item.price}
                    </View>
                    <View className='shop-shuliang'>
                       
                        <View className='goods-shuliang-right'>
                            <Text data-num={item.number} data-index={index}  onClick={this.delc} className='fa fa-minus-square fa-lg'></Text>
                            <Text className='goods-shuliang-zonghe'>{item.number}</Text>
                            <Text data-num={item.number} data-index={index} onClick={this.infc}  className='fa fa-plus-square fa-lg'></Text>
                        </View>
                    </View>
                  </View>
                  
                </View>
              </View> 
            </View>
           </View>
           
           )
         }
         <View style={{height:'100px'}} />
       <View className='shop-btm'>
          <View className="checkbox-box" onClick={this.allSelect}>
            <View className="checkbox">
             <View className="{{isAllselected?'on':''}}"></View>
            </View>
            <View className='shop-btm-flex'>全选</View>
          </View>
          {
            this.state.selectedList.length>0?(<View className="delBtn show" onClick={this.deleteList} >删除</View>):(<View className="delBtn hiden" onClick={this.deleteList} >删除</View>)
          }
         
          <View className='shop-btm-pay'>
            合算:<Text>￥:{this.state.sumPrice}</Text>
            <View className='shop-btm-balace' onClick={this.toConfirmation}>结算</View>
          </View>
         
       </View>
     </View>
      
    )
  }
}
export default Shop;