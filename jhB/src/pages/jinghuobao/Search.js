import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './Classify.css'
import './search.css'
class Search extends Component {
config: Config = {
    navigationBarTitleText: '搜索'
  }
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='search'>
        <View className="classify-view"><Icon className="classify-icon" size='15' type='search' /><Input  type="text" className="classify-search" placeholder='请输入搜索关键词'/></View>
        <View className='search-view'>热门搜索</View>
        <Text className='search-text'>手机</Text>
        <Text className='search-text'>手机</Text>
        <Text className='search-text'>手机</Text>
        <Text className='search-text'>手机</Text>
        <View className='search-view'>搜索历史</View>
        <Text className='search-text1'>电视</Text>
        <Text className='search-text1'>电视</Text>
        <View className='search-bt'>清空历史记录</View>
      </View>
    )
  }
}
export default Search;