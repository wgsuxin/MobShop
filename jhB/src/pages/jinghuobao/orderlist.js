import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon } from '@tarojs/components'

import no from './img/noorder.png'
import p1 from './img/goods/p1.jpg'
import p2 from './img/goods/p2.jpg'
import p3 from './img/goods/p3.jpg'
import p4 from './img/goods/p4.jpg'
import p5 from './img/goods/p5.jpg'
import p6 from './img/goods/p6.jpg'
import './orderlist.css'

class Orderlist extends Component{
    constructor(props){
        super(props)
        this.state={
            headerPosition:"fixed",
            tabarIndex:0,
            headerTop:"0px",
            typeText:{
            	unpaid:'等待付款',
            	back:'等待商家发货',
            	unreceived:'商家已发货',
            	received:'等待用户评价',
            	completed:'交易已完成',
            	refunds:'商品退货处理中'
            },
            orderType: ['全部','待付款','待发货','待收货','待评价','退换货'],
            
            orderList:[
            	[
            		{ type:"unpaid",ordersn:0,goods_id: 0, img: p1, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"unpaid",ordersn:1,goods_id: 1, img:p2, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"back",ordersn:1,goods_id: 1, img:p3, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"unreceived",ordersn:1,goods_id: 1, img: p4, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"received",ordersn:1,goods_id: 1, img:p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"completed",ordersn:1,goods_id: 1, img: p6, name: '商品名称商品名称商品名称商品名称商品名称', price: '168.00',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"refunds",ordersn:1,goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 }
            	],
            	[
            		{ type:"unpaid",ordersn:0,goods_id: 0, img: p1, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 },
            		{ type:"unpaid",ordersn:1,goods_id: 1, img:p2, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 }
            	],
            	[
            		//无
            	],
            	[
            		{ type:"unreceived",ordersn:1,goods_id: 1, img: p4, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 }
            	],
            	[
            		{ type:"received",ordersn:1,goods_id: 1, img:p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 }
            	],
            	[
            		{ type:"refunds",ordersn:1,goods_id: 1, img: p5, name: '商品名称商品名称商品名称商品名称商品名称', price: '￥168',payment:168.00,freight:12.00,spec:'规格:S码',numner:1 }
            	]
            	
            ],
            list:[],
            loading: true
            
        }
        this.showType=this.showType.bind(this)
        
    }
    
componentDidMount(){
    const itemlist=this.state.orderList
     const that = this
    setTimeout(function  () {
        that.setState({
          loading: false,
          list:itemlist[that.state.tabarIndex]
        })
    	console.log(that.state.list)
    } ,1500);
    console.log(this.state.list)
}
   showType(tbIndex){
     
     const index = tbIndex.currentTarget.dataset.id
     const itemlists=[...this.state.orderList]
     this.setState({
       tabarIndex:index,
       list:itemlists[index]
     });
    
     console.log(this.state.list)
    
   } 
    
    render(){
      const {orderType}=this.state
      const {list}=this.state
        return(
        <View>
            <View className="topTabBar" style={{position:'headerPosition',top:'headerTop'}}>
            {
               orderType.map((grid,tbIndex)=>
                   
              <View data-id={tbIndex} className="grid" onClick={this.showType}>
            		<View className="text {{tbIndex==tabarIndex?'on':''}}">{grid}
                    </View>
              </View>
                   )
            }
            </View>   	
         
         
         <View className="order-list">
         	<View className="list">
         {
             list.length!==0?'':(
              <View className="onorder listshow" >
                <Image className="onorderimg" src={no} mode="widthFix"></Image>
                <View className="text1">
                    {loading?'正在加载。。。':'没有相关订单'}
                </View>
              </View>  
             
             )
         }
                {
                list.map((item,tabindex)=>
                
                
                <View className="row" key={tabindex}>
                 
                  
                    <View className="type {{list.length!==0?'listshow':'listhidden'}}">{this.state.typeText[item.type]}</View>
                    <View className="order-info">
                        <View className="left">
                            <Image className="leftimg" src={item.img} mode="widthFix"></Image>
                        </View>
                        <View className="right">
                            <View className="name">
                                {item.name}{list.length}
                            </View>
                            <View className="spec">{item.spec}</View>
                            <View className="price-number">
                                ￥<View className="price">{item.price}</View>
                                x<View className="number">{item.numner}</View>
                            </View>
                        </View>
                        
                    </View>
                    <View className="detail">
                        <View className="number">共{item.numner}件商品</View>
                        <View className="sum">合计￥<View className="price">{item.payment}</View>
                    </View>
                    <View className="nominal">(含运费 ￥{item.freight})</View>
                    </View> 
                 
                  
                  <View className="btns">
                  {
                      item.type=='unpaid'?(
                      <View className="listshow"><View className="default">取消订单</View><View className="pay">付款</View></View>
                      ):''
                  }
                   {
                      item.type=='back'?(
                      <View className="listshow"><View className="default">提醒发货</View></View>
                      ):''
                  }
                  {
                      item.type=='unreceived'?(
                      <View className="listshow"><View className="default">查看物流</View><View className="pay">确认收货</View><View className="pay">我要退货</View></View>
                      ):''
                  }
                  {
                      item.type=='received'?(
                      <View className="listshow"><View className="default">评价</View><View className="default">再次购买</View></View>
                      ):''
                  }
                  {
                      item.type=='completed'?(
                      <View className="listshow"><View className="default">再次购买</View></View>
                      ):''
                  }
                  {
                      item.type=='refunds'?(
                      <View className="listshow"><View className="default">查看进度</View></View>
                      ):''
                  }
                  </View>
                  
                  
                </View>      

               
                
               ) }
            </View>
         </View>
       </View>
        
        )
    }
    
    
}
export default Orderlist;