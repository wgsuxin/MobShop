import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem  } from '@tarojs/components'
import './goodslist.css'
import right from '../img/right.png'
import hua from '../img/hua.png'
import p1 from '../img/goods/p1.jpg'
import p2 from '../img/goods/p2.jpg'
import p3 from '../img/goods/p3.jpg'
import p4 from '../img/goods/p4.jpg'
import p5 from '../img/goods/p5.jpg'
import p6 from '../img/goods/p6.jpg'
import p7 from '../img/goods/p7.jpg'
import p8 from '../img/goods/p8.jpg'
import p9 from '../img/goods/p9.jpg'
import p10 from '../img/goods/p10.jpg'


class Publicgoodslist extends Component {
 constructor() {
    super(...arguments)
    this.state={
       productList: [
      	{
      		goods_id: 0,
      		img: p1,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 1,
      		img: p2,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 2,
      		img: p3,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 3,
      		img:p4,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 4,
      		img:p5,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 5,
      		img: p6,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 6,
      		img: p7,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 7,
      		img: p8,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 8,
      		img:p9,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	},
      	{
      		goods_id: 9,
      		img:p10,
      		name: '商品名称商品名称商品名称商品名称商品名称',
      		price: '￥168',
      		slogan: '1235人付款'
      	}
      ],
     
    }
   this.toGoods=this.toGoods.bind(this)
  }
 
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
toGoods(goods){
  const good=goods.currentTarget.dataset.id
 console.log(good)
 this.$preload('key', 'val')
  Taro.navigateTo({
    url:"/pages/jinghuobao/goods?url="+good
  })
}

  render () {
    return (
      
        	<View className="product-list">
                  {this.state.productList.map((product)=>
                    <View className="product"
                     key={product.goods_id}
                      data-id={product} 
                      onClick={this.toGoods}
                      >
                        <Image mode="widthFix" src={product.img}></Image>
                        <View className="name">{ product.name }</View>
                        <View className="info">
                            <View className="price">{product.price}</View>
                            <View className="slogan">{product.slogan }</View>
                        </View>
                    </View>
                  )}
        	</View>
        	
    )
  }
}
export default Publicgoodslist;