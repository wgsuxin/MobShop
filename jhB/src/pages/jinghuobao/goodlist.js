import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon , Swiper, SwiperItem,Image  } from '@tarojs/components'
import Searchtop from './searchtop'
import { AtIcon } from 'taro-ui'
import right from './img/right.png'
import p1 from './img/goods/p1.jpg'
import p3 from './img/goods/p3.jpg'
import p4 from './img/goods/p4.jpg'
import p5 from './img/goods/p5.jpg'
import './prolist.css'
import './goods.css'


class GoodsList extends Component {
 constructor() {
    super(...arguments)
    this.state={
       sumPrice:'0.00',
       	headerPosition:"fixed",
       	headerTop:null,
       	statusTop:null,
       	selectedList:[],
       	isAllselected:false,
       	goodsList:[
       		{id:1,img:p1,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1,selected:false},
       		{id:2,img:p2,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1,selected:false},
       		{id:3,img:p3,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1,selected:false},
       		{id:4,img:p4,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1, :false},
       		{id:5,img:p5,name:'商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题',spec:'规格:S码',price:127.5,number:1,selected:false}
       	],
       	//控制滑动效果
       	theIndex:null,
       	oldIndex:null,
       	isStop:false
       }
    this.handshow = this.handshow.bind(this)
    this.discard = this.discard.bind(this)
    this.sum = this.sum.bind(this)
    this.allSelect = this.allSelec.bind(this)
    this.deleteList = this.deleteList.bind(this)
  }
 


  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  
  
  
  
  onPageScroll(e){
  	//兼容iOS端下拉时顶部漂移
  	this.headerPosition = e.scrollTop>=0?"fixed":"absolute";
  	this.headerTop = e.scrollTop>=0?null:0;
  	this.statusTop = e.scrollTop>=0?null:-this.statusHeight+'px';
  },
  //下拉刷新，需要自己在page.json文件中配置开启页面下拉刷新 "enablePullDownRefresh": true
  onPullDownRefresh() {
      setTimeout(function () {
          uni.stopPullDownRefresh();
      }, 1000);
  },
touchStart(index,event){
				//多点触控不触发
				if(event.touches.length>1){
					this.isStop = true;
					return ;
				}
				this.oldIndex = this.theIndex;
				this.theIndex = null;
				//初始坐标
				this.initXY = [event.touches[0].pageX,event.touches[0].pageY];
			},
			touchMove(index,event){
				//多点触控不触发
				if(event.touches.length>1){
					this.isStop = true;
					return ;
				}
				let moveX = event.touches[0].pageX - this.initXY[0];
				let moveY = event.touches[0].pageY - this.initXY[1];
				
				if(this.isStop||Math.abs(moveX)<5){
					return ;
				}
				if (Math.abs(moveY) > Math.abs(moveX)){
					// 竖向滑动-不触发左滑效果
					this.isStop = true;
					return;
				}
				
				if(moveX<0){
					this.theIndex = index;
					this.isStop = true;
				}else if(moveX>0){
					if(this.theIndex!=null&&this.oldIndex==this.theIndex){
						this.oldIndex = index;
						this.theIndex = null;
						this.isStop = true;
						setTimeout(()=>{
							this.oldIndex = null;
						},150)
					}
				}
			},
			touchEnd(index,$event){
				//结束禁止触发效果
				this.isStop = false;
			},
			//控制左滑删除效果-end
			
			
			//商品跳转
			toGoods(e){
				uni.showToast({title: '商品'+e.id,icon:"none"});
				uni.navigateTo({
					url: '../goods/goods' 
				});
			},
			//跳转确认订单页面
			toConfirmation(){
				let tmpList=[];
				let len = this.goodsList.length;
				for(let i=0;i<len;i++){
					if(this.goodsList[i].selected) {
						tmpList.push(this.goodsList[i]);
					}
				}
				if(tmpList.length<1){
					uni.showToast({
						title:'请选择商品结算',
						icon:'none'
					});
					return ;
				}
				uni.setStorage({
					key:'buylist',
					data:tmpList,
					success: () => {
						uni.navigateTo({
							url:'../order/confirmation'
						})
					}
				})
			},
			//删除商品
			deleteGoods(id){
				let len = this.goodsList.length;
				for(let i=0;i<len;i++){
					if(id==this.goodsList[i].id){
						this.goodsList.splice(i, 1);
						break;
					}
				}
				this.sum();
				this.oldIndex = null;
				this.theIndex = null;
			},
			// 删除商品s
			deleteList(){
				let len = this.selectedList.length;
				for(let i=0;i<len;i++){
					this.deleteGoods(this.selectedList[i]);
				}
				this.selectedList = [];
				this.isAllselected = this.selectedList.length == this.goodsList.length && this.goodsList.length>0;
				this.sum();
			},
			// 选中商品
			selected(index){
				this.goodsList[index].selected = this.goodsList[index].selected?false:true;
				let i = this.selectedList.indexOf(this.goodsList[index].id);
				i>-1?this.selectedList.splice(i, 1):this.selectedList.push(this.goodsList[index].id);
				this.isAllselected = this.selectedList.length == this.goodsList.length;
				this.sum();
			},
			//全选
			allSelect(){
				let len = this.goodsList.length;
				let arr = [];
				for(let i=0;i<len;i++){
					this.goodsList[i].selected = this.isAllselected? false : true;
					arr.push(this.goodsList[i].id);
				}
				this.selectedList = this.isAllselected?[]:arr;
				this.isAllselected = this.isAllselected||this.goodsList.length==0?false : true;
				this.sum();
			},
			// 减少数量
			sub(index){
				if(this.goodsList[index].number<=1){
					return;
				}
				this.goodsList[index].number--;
				this.sum();
			},
			// 增加数量
			add(index){
				this.goodsList[index].number++;
				this.sum();
			},
			// 合计
			sum(){
				this.sumPrice=0;
				let len = this.goodsList.length;
				for(let i=0;i<len;i++){
					if(this.goodsList[i].selected) {
						this.sumPrice = this.sumPrice + (this.goodsList[i].number*this.goodsList[i].price);
					}
				}
				this.sumPrice = this.sumPrice.toFixed(2);
			},
			discard() {
				//丢弃
			}
			
  render () {
    return (
      <View>
      	<View>
      		<View className="status" style={{position:headerPosition,top:statusTop}}></View>
      		<View className="header" style={{position:headerPosition,top:headerTop}}>
      			<View className="title">购物车</View>
      		</View>
      		{/* 占位*/}
      		<View className="place"></View>
      		{/* 商品列表*/}
      		<View className="goods-list">
      			{
                   this.state.goodsList.map((row,index)=>{
                       
                       <View className="row" key={index} >
                       	{/* 删除按钮*/}
                       	<View className="menu" onClick={this.deleteGoods.bind(this,row.id)}>
                       		<View className="icon shanchu"></View>
                       	</View>
                       	{/* 商品*/}
                       	<View className="carrier" className="{{theIndex==index?'open':oldIndex==index?'close':''}}" onTouchStart={this.touchStart.bind(this,index,$event)} onTouchMove={this.touchMove.bind(this,index,$event)} onTouchEnd={this.touchEnd.bind(this,index,$event)}>
                       		{/* checkbox*/}
                       		<View className="checkbox-box" onClick={this.selected.bind(this,index)}>
                       			<View className="checkbox">
                       				<View className="{{row.selected?'on':''}}"></View>
                       			</View>
                       		</View>
                       		{/* 商品信息*/}
                       		<View className="goods-info" onClick={this.toGoods.bind(this,row)}>
                       			<View className="img">
                       				<Image src={row.img}></Image>
                       			</View>
                       			<View className="info">
                       				<View className="title">{row.name}</View>
                       				<View className="spec">{row.spec}</View>
                       				<View className="price-number">
                       					<View className="price">￥{row.price}</View>
                       					<View className="number">
                       						<View className="sub" onClick={this.sub.bind(this,index)}>
                       							<View className="icon jian"></View>
                       						</View>
                       						<View className="input" onClick={this.discard}>
                       							<input type="number" value={row.number} onChange={this.sum} />
                       						</View>
                       						<View className="add"  onClick={this.add.bind(this,index)}>
                       							<View className="icon jia"></View>
                       						</View>
                       					</View>
                       				</View>
                       			</View>
                       		</View>
                       	</View>
                       </View>
                       
                   }
                   ) 
                    
                }
      	        
      	    </View>
      		{/* 脚部菜单*/}
      		<View className="footer" style={{bottom:footerbottom}}>
      			<View className="checkbox-box" onClick={this.allSelect}>
      				<View className="checkbox">
      					<View className="{{isAllselected?'on':''}}"></View>
      				</View>
      				<View className="text">全选</View>
      			</View>
      			<View className="delBtn" onClick={this.deleteList} v-if="selectedList.length>0">删除</View>
      			<View className="settlement">
      				<View className="sum">合计:<View className="money">￥{sumPrice}</View></View>
      				<View className="btn" onClick={this.toConfirmation}>结算({selectedList.length})</View>
      			</View>
      		</View>
      	</View>
    )
  }
}
export default GoodsList;