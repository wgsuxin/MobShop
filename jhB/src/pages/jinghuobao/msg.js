import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text , Input,Icon } from '@tarojs/components'
import './msg.css'
import face1 from './img/im/face/face_1.jpg'
import face2 from './img/im/face/face_2.jpg'
import face3 from './img/im/face/face_3.jpg'
import face4 from './img/im/face/face_4.jpg'
import face5 from './img/im/face/face_5.jpg'
import face6 from './img/im/face/face_6.jpg'
import face7 from './img/im/face/face_7.jpg'
import face8 from './img/im/face/face_8.jpg'
import face9 from './img/im/face/face_9.jpg'
import face10 from './img/im/face/face_10.jpg'
import face11 from './img/im/face/face_11.jpg'
import face12 from './img/im/face/face_12.jpg'
import face13 from './img/im/face/face_13.jpg'
import face15 from './img/im/face/face_15.jpg'

class Msg extends Component {
    constructor(props){
        super(props)
        this.state={
            chatList:[
            	{
            		uid:1,
            		username:"鲜果蔬专营店",
            		face:face1,
            		time:"13:08",
            		msg:"亲，20点前下单都是当天送达的",
            		tisNum:1
            	},
            	{
            		uid:2,
            		username:"官店大欺客旗舰店",
            		face:face2,
            		time:"13:05",
            		msg:"问那么多干什么？不想买就滚~",
            		tisNum:0
            	},
            	{
            		uid:3,
            		username:"妙不可言",
            		face:face3,
            		time:"12:15",
            		msg:"推荐一个商品呗？",
            		tisNum:0
            	},
            	{
            		uid:4,
            		username:"茶叶专卖",
            		face:face4,
            		time:"12:11",
            		msg:"现在卖的都是未过青的茶哦",
            		tisNum:0
            	},
            	{
            		uid:5,
            		username:"likeKiss客服",
            		face:face5,
            		time:"12:10",
            		msg:"你好，发福建快递多久送到的？",
            		tisNum:0
            	},
            	{
            		uid:6,
            		username:"白开水",
            		face:face6,
            		time:"12:10",
            		msg:"在吗？",
            		tisNum:0
            	},
            	{
            		uid:7,
            		username:"衣帽间的叹息",
            		face:face7,
            		time:"11:56",
            		msg:"新品上市，欢迎选购",
            		tisNum:0
            	},
            	{
            		uid:8,
            		username:"景萧疏",
            		face:face8,
            		time:"11:56",
            		msg:"商品七天无理由退换的",
            		tisNum:0
            	},
            	{
            		uid:9,
            		username:"文宁先生",
            		face:face9,
            		time:"12:15",
            		msg:"星期天再发货的",
            		tisNum:0
            	},
            	{
            		uid:10,
            		username:"高端Chieh",
            		face:face10,
            		time:"12:36",
            		msg:"建议你直接先测量好尺码在选购的。",
            		tisNum:0
            	},
            	{
            		uid:11,
            		username:"mode旗舰店",
            		face:face11,
            		time:"12:40",
            		msg:"新品5折，还有大量优惠券。",
            		tisNum:0
            	},
            	{
            		uid:12,
            		username:"敏萘客服",
            		face:face12,
            		time:"12:36",
            		msg:"还没有用，等我明天早上试一下",
            		tisNum:0
            	},
            	{
            		uid:13,
            		username:"春天里的花",
            		face:face13,
            		time:"12:36",
            		msg:"适用于成年人的，小孩不适用的",
            		tisNum:0
            	},
            	{
            		uid:14,
            		username:"电脑外设专业户",
            		face:face13,
            		time:"12:36",
            		msg:"把上面的螺丝拆下来，把铁片撬开就能看见了",
            		tisNum:0
            	},
            	{
            		uid:15,
            		username:"至善汽车用品",
            		face:face15,
            		time:"12:36",
            		msg:"这个产品是原车配件，完美装上的",
            		tisNum:0
            	}
            
            ]
        }
    }
    
    render(){
        const {chatList}=this.state
    return(
     <View className="chat-list">
     {
       chatList.map((chat,index)=>
     		<View className="chat" key={index}>
     			<View className="row">
     				<View className="left">
     					<Image src={chat.face} mode="widthFix"></Image>
     				</View>
     				<View className="right">
     					<View className="top">
     						<View className="username">{chat.username}</View>
     						<View className="time">{chat.time}</View>
     					</View>
     					<View className="bottom">
     						<View className="msg">{chat.msg}</View>
     						<View className="tis">{chat.tisNum}</View>
     					</View>
     				</View>
     			</View>
     		</View>
     	
        )}
     </View>
  
    
    
    )
    
    }
    
}
export default Msg;